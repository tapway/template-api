import Promise from "bluebird";
import mongoose from "mongoose";
import config from "../config/env";
import server from "../config/express";
import startSocketServer from "../config/socket";

// Promisify All The Mongoose
Promise.promisifyAll(mongoose);

console.log("Server is Running......:-)");

// Connecting Mongo DB
mongoose.connect(config.db, {
  reconnectInterval: 500,
  bufferMaxEntries: 0,
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});
mongoose.connection.on("error", () => {
  throw new Error(`unable to connect to database: ${config.db}`);
});

startSocketServer(server);

const debug = require("debug")("express-mongoose-es6-rest-api:index");

// listen on port config.port
server.listen(process.env.PORT || config.port, () => {
  debug(`.........server started on port ${config.port} (${config.env})`);
});

export default server;
