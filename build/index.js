"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _bluebird = _interopRequireDefault(require("bluebird"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _env = _interopRequireDefault(require("../config/env"));

var _express = _interopRequireDefault(require("../config/express"));

var _socket = _interopRequireDefault(require("../config/socket"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Promisify All The Mongoose
_bluebird["default"].promisifyAll(_mongoose["default"]);

console.log("Server is Running......:-)"); // Connecting Mongo DB

_mongoose["default"].connect(_env["default"].db, {
  reconnectInterval: 500,
  bufferMaxEntries: 0,
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

_mongoose["default"].connection.on("error", function () {
  throw new Error("unable to connect to database: ".concat(_env["default"].db));
});

(0, _socket["default"])(_express["default"]);

var debug = require("debug")("express-mongoose-es6-rest-api:index"); // listen on port config.port


_express["default"].listen(process.env.PORT || _env["default"].port, function () {
  debug(".........server started on port ".concat(_env["default"].port, " (").concat(_env["default"].env, ")"));
});

var _default = _express["default"];
exports["default"] = _default;
module.exports = exports.default;
//# sourceMappingURL=index.js.map