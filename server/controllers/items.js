import Items from "../models/items";
import mongoose from "mongoose";
import _ from "lodash";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/** Get All Items
 * @param req
 * @param res
 * @param next
 * @returns {Array of items}
 */
function getAll(req, res, next) {
  return Items.findAsync()
    .then(result => {
      res.send({ success: true, message: "found all Items", data: result });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in getting all items ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "All Items Not Fetched"
      });
      next(err);
    });
}

/** Get All Items By Shop
 * @param req
 * @param res
 * @param next
 * @returns {Array of items}
 */
function getAllByShop(req, res, next) {
  const result = Items.list(
    req.body.skip,
    req.body.limit,
    req.body.shopId,
    req.body.itemType
  );
  result
    .then(data => {
      return res.json({
        success: true,
        message: "Limited Items Fetched",
        data
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in getting items by shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "All Items By Shop Not Fetched"
      });
      next(err);
    });
}

/** Get All Items By Shop With Query
 * @param req
 * @param res
 * @param next
 * @returns {Array of items}
 */
function getAllByShopWithQuery(req, res, next) {
  const result = Items.listByQuery(
    req.body.skip,
    req.body.limit,
    req.body.shopId,
    req.body.itemType,
    req.body.query
  );
  result
    .then(data => {
      return res.json({
        success: true,
        message: "Limited Items Fetched With Filter",
        data
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in getting all items by shop with query ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "All Items By Shop With Query Not Fetched"
      });
      next(err);
    });
}

/** Get All Items By Shop With Filters
 * @param req
 * @param res
 * @param next
 * @returns {Array of items}
 */
function getAllByShopWithFilters(req, res, next) {
  const result = Items.listByFilters(
    req.body.skip,
    req.body.limit,
    req.body.shopId,
    req.body.itemType,
    req.body.query,
    req.body.filters
  );
  result
    .then(data => {
      return res.json({
        success: true,
        message: "Limited Items Fetched With Filter",
        data
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in getting all items by shop with filters ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "Limited Items By Shop With Filters Not Fetched"
      });
      next(err);
    });
}

/** Get All Filter Requirements By Shop
 * @param req
 * @param res
 * @param next
 * @returns {Array of items}
 */
function getFilterRequirementsByShop(req, res, next) {
  const id = req.body.id;
  Items.findAsync({ shopId: id })
    .then(list => {
      const result = _.uniq(_.map(list, "brand"));
      const min = Math.min(..._.map(list, "price"));
      const max = Math.max(..._.map(list, "price"));
      return res.json({
        success: true,
        message: "Brand names fetched",
        data: {
          brandList: result,
          minPrice: min,
          maxPrice: max
        }
      });
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in getting filter requirements by shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: `error in fetching brand names :${errMessage}`
      });
      next(err);
    });
}

/** Delete All Items
 * @param req
 * @param res
 * @param next
 * @returns {Array of items}
 */
function deleteAll(req, res, next) {
  return Items.deleteMany({})
    .execAsync()
    .then(result => {
      res.send({ success: true, message: "deleted all Items", data: result });
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in getting filter requirements by shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.send({ success: false, message: "All items not deleted" });
      next(err);
    });
}

/** Create Item
 * @param req
 * @param res
 * @param next
 * @returns {Deleted Count}
 */
function create(req, res, next) {
  const item = new Items({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    pic: req.body.pic,
    itemType: req.body.itemType,
    shopId: req.body.shopId
  });
  return item
    .saveAsync()
    .then(savedItem => {
      res.send({
        success: true,
        message: "Item saved",
        data: { savedItem }
      });
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in getting filter requirements by shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

/** Update Item
 * @param req
 * @param res
 * @param next
 * @returns {Updated Item}
 */
function update(req, res, next) {
  return Items.findById(req.body.id)
    .execAsync()
    .then(item => {
      item.name = req.body.name ? req.body.name : item.name;
      item.price = req.body.price ? req.body.price : item.price;
      item.pic = req.body.pic ? req.body.pic : item.pic;
      item.itemType = req.body.itemType ? req.body.itemType : item.itemType;
      item.shopId = req.body.shopId ? req.body.shopId : item.shopId;
      item
        .saveAsync()
        .then(savedItem => {
          res.send(savedItem);
        })
        .catch(errMessage => {
          const err = new APIError(
            `error in updating item ${errMessage}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          next(err);
        });
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in finding item for update ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

/** Delete Item By Id
 * @param req
 * @param res
 * @param next
 * @returns {Deleted Item}
 */
function deleteById(req, res, next) {
  return Items.deleteOne({ _id: req.body.id })
    .execAsync()
    .then(result => res.send(result))
    .catch(errMessage => {
      const err = new APIError(
        `error in deleting item by id ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

export default {
  getAll,
  getAllByShop,
  create,
  update,
  deleteAll,
  deleteById,
  getAllByShopWithQuery,
  getAllByShopWithFilters,
  getFilterRequirementsByShop
};
