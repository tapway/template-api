import jwt from "jsonwebtoken";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import sendMessage from "../service/sendMessage";
import DeliveryUserSchema from "../models/deliveryuser";
const config = require("../../config/env");

/**
 * Will return random number.
 * @returns {otp}
 */
function getRandomNumber() {
  const otp = Math.floor(1000 + Math.random() * 9000);
  return otp;
}

/**
 * This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
function login(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  DeliveryUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        if(user.isLoggedIn)
        {
          const returnObj = {
            success: false,
            errorCode:1,
            message: "User not found"
          };
          res.json(returnObj);
        }
        else{
          const token = getRandomNumber();
          sendMessage(req.body.phoneNumber, token);
          DeliveryUserSchema.findOneAndUpdateAsync(
            { _id: user._id },
            { $set: { otp: token } },
            { new: true }
          )
            .then(updatedUser => {
              const returnObj = {
                success: true,
                message: "User successfully logged in"
              };
              res.json(returnObj);
            })
            .error(errMessage => {
              const err = new APIError(
                `error in logging delivery user ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
              );
              res.json({
                success: false,
                message: "error in logging delivery user"
              });
              next(err);
            });
        }
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in while finding delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in while finding delivery user"
      });
      next(err);
    });
}

/**
 * Returns jwt token if valid otp is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function otpVerify(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber,
    otp: req.body.otp
  };
  DeliveryUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        var i = "Teniola";
        var signOptions = {
          issuer: i,
          algorithm: "HS256"
        };
        const token = jwt.sign(
          { _doc: { _id: user._id, type: "deliveryUser" } },
          config.jwtSecret,
          signOptions
        );
        DeliveryUserSchema.findOneAndUpdateAsync(
          { _id: user._id },
          {
            $set: {
              isLoggedIn: true,
              isAvailable: false,
              deviceInfo: req.body.deviceInfo ? req.body.deviceInfo : null
            }
          },
          { new: true }
        )
          .then(updatedUser => {
            const returnObj = {
              success: true,
              message: "User successfully logged in",
              data: {
                jwtAccessToken: `JWT ${token}`,
                user: updatedUser
              }
            };
            res.json(returnObj);
          })
          .error(errMessage => {
            const err = new APIError(
              `error in otp verification of delivery user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
              success: false,
              message: "error in otp verification of delivery user"
            });
            next(err);
          });
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in finding delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in finding delivery user"
      });
      next(err);
    });
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function resendOtp(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  DeliveryUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        sendMessage(req.body.phoneNumber, user.otp);
        const returnObj = {
          success: true,
          message: "Otp send"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in resending otp to delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in resending otp to delivery user"
      });
      next(err);
    });
}

/**
 * Returns true if user exists
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function checkUserExistance(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  DeliveryUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User does not exist"
        };
        res.json(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: "User exists already"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in checking user in db ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in checking user in db"
      });
      next(err);
    });
}

/** This is a protected route. Change login status to false and send success message.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function logout(req, res, next) {
  DeliveryUserSchema.findOneAndUpdate(
    { _id: req.user._id },
    { $set: { isLoggedIn: false, isAvailable: false } },
    { new: true },
    (err, userDoc) => {
      if (err) {
        const error = new APIError(
          "error while updateing login status",
          httpStatus.INTERNAL_SERVER_ERROR
        );
        next(error);
      }
      if (userDoc) {
        const returnObj = {
          success: true,
          message: "User logout successfully"
        };
        res.json(returnObj);
      } else {
        const error = new APIError("user not found", httpStatus.NOT_FOUND);
        next(error);
      }
    }
  );
}

export default { login, logout, otpVerify, checkUserExistance, resendOtp };
