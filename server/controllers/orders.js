import Orders from "../models/orders";
import APIError from "../helpers/APIError";
import httpStatus from "http-status";

/**
 * Load Order By Id
 * @set {req.order}
 */
function load(req, res, next, orderId) {
    req.orderId = orderId;
    Orders.get(orderId)
        .then(order => {
            req.order = order;
            return next();
        })
        .error(errMessage => {
            const err = new APIError(
                `error in loading Order ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
                success: false,
                message: "Order not loaded"
            });
            next(err);
        });
}

/**
 * Get Order
 * @returns {Order}
 */
function get(req, res) {
    return res.json({
        success: true,
        message: "Order found",
        data: {order: req.order}
    });
}

/** Get All Orders
 * @param req
 * @param res
 * @param next
 * @returns {Array of Orders}
 */
function getAll(req, res, next) {
    return Orders.findAsync()
        .then(result => res.json(result))
        .error(errMessage => {
            const err = new APIError(
                `error in getting all Orders ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
                success: false,
                message: "All Orders Not Fetched"
            });
            next(err);
        });
}

function checkPrice() {

}

/** Get All Orders By Delivery Id
 * @param req
 * @param res
 * @param next
 * @returns {Array of Orders}
 */
function fetchByDeliveryId(req, res, next) {
    return Orders.find({deliveryId: req.params.deliveryId})
        .execAsync()
        .then(result =>
            res.json({success: true, message: "found orders", data: result})
        )
        .error(errMessage => {
            const err = new APIError(
                `error in getting orders by delivery guy id ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
                success: false,
                message: "not able to find orders"
            });
            next(err);
        });
}

/** Get All Orders By User Id
 * @param req
 * @param res
 * @param next
 * @returns {Array of Orders}
 */
function fetchByUser(req, res, next) {
    return Orders.find({userId: req.params.userId})
        .execAsync()
        .then(result =>
            res.json({success: true, message: "found orders", data: result})
        )
        .error(errMessage => {
            const err = new APIError(
                `error in getting orders by end user id ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
                success: false,
                message: "not able to find orders"
            });
            next(err);
        });
}

/** Get Limited Orders
 * @param req
 * @param res
 * @param next
 * @returns {Array of Orders}
 */
function fetchLimitedOrders(req, res, next) {
    if (req.body.userId) {
        const result = Orders.list(
            req.body.skip,
            req.body.limit,
            req.body.userId,
            "endUser"
        );
        result
            .then(data => {
                return res.json({
                    success: true,
                    message: "Limited Orders Fetched",
                    data
                });
            })
            .error(errMessage => {
                const err = new APIError(
                    `error in getting limited orders by end user id ${errMessage}`,
                    httpStatus.INTERNAL_SERVER_ERROR
                );
                res.json({
                    success: false,
                    message: "not able to find orders"
                });
                next(err);
            });
    } else {
        const result = Orders.list(
            req.body.skip,
            req.body.limit,
            req.body.deliveryId,
            "delivery"
        );
        result
            .then(data => {
                return res.json({
                    success: true,
                    message: "Limited Orders Fetched",
                    data
                });
            })
            .error(errMessage => {
                const err = new APIError(
                    `error in getting limited orders by delivery guy id ${errMessage}`,
                    httpStatus.INTERNAL_SERVER_ERROR
                );
                res.json({
                    success: false,
                    message: "not able to find orders"
                });
                next(err);
            });
    }
}




/** Delete All Orders
 * @param req
 * @param res
 * @param next
 * @returns {Deleted Count}
 */
function deleteAll(req, res, next) {
    return Orders.deleteMany({})
        .execAsync()
        .then(del => res.json(del))
        .error(errMessage => {
            const err = new APIError(
                `error in deleting all orders ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
                success: false,
                message: "not able to delete all orders"
            });
            next(err);
        });
}

/** Verify Ride
 * @param req
 * @param res
 * @param next
 * @returns {success}
 */
function verifyRide(req, res, next) {
    const rideObj = {
        otp: req.body.otp,
        _id: req.body.orderId
    };
    Orders.findOneAsync(rideObj)
        .then(order => {
            if (!order) {
                const returnObj = {
                    success: false,
                    message: "Ride Not Verified"
                };
                res.json(returnObj);
            } else {
                const returnObj = {
                    success: true,
                    message: "Ride Verified"
                };
                res.json(returnObj);
            }
        })
        .error(e => {
            const err = new APIError(
                `error while verifing ride ${e}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            next(err);
        });
}
function cancel(req, res, next) {

    const changedEntry = {
        isCanceled: true,
        cancelReason: req.body.cancelReason,
    };

    Orders.findByIdAndUpdate(req.body._id, changedEntry, {new: true}, (err, model) => {
        if (err) {
            return res.status(500).json({message: 'Error while canceling'});
        }
        return res.status(200).json({
            success: true,
            message: 'Cancel Successful',
            data: {model}
        });
    });
}

/** Create Order
 * @param req
 * @param res
 * @param next
 * @returns {Saved Order}
 */
function create(req, res, next) {
    var date = new Date();
    var timestamp = date.getTime();
    const order = new Orders({
        userId: req.body.userId,
        deliveryId: req.body.deliveryId,
        deliveryLocation: req.body.deliveryLocation,
        pickUpLocation: req.body.pickUpLocation,
        driverGpsLocation: req.body.driverGpsLocation,
        processingStatus: req.body.processingStatus,
        deliveryAddress: req.body.deliveryAddress,
        otp: Math.floor(1000 + Math.random() * 9000),
        paymentType: req.body.paymentType,
        deliveryInstructions: req.body.deliveryInstructions,
        paymentAmount: req.body.paymentAmount,
        userRating: req.body.userRating,
        deliveryRating: req.body.deliveryRating,
        userReview: req.body.userReview,
        deliveryReview: req.body.deliveryReview,
        shopReview: req.body.shopReview,
        shopRating: req.body.shopRating,
        anyIssue: req.body.anyIssue,
        pickUpAddress: req.body.pickUpAddress,
        category: req.body.category,
        deliveredDateTime: timestamp,
        createdAt: timestamp,
        updatedAt: timestamp
    });
    order
        .saveAsync()
        .then(savedorder => {
            const returnObj = {
                success: true,
                message: "saved order",
                data: {savedorder}
            };
            res.json(returnObj);
        })
        .error(errMessage => {
            const err = new APIError(
                `error in creating order ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({success: false, message: "not able to create order"});
            next(err);
        });
}

/** Update order
 * @param req
 * @param res
 * @param next
 * @returns {Update Order}
 */
function update(req, res, next) {
    const order = req.order;
    order.userId = req.body.userId ? req.body.userId : order.userId;
    order.deliveryId = req.body.deliveryId
        ? req.body.deliveryId
        : order.deliveryId;
    order.deliveryLocation = req.body.deliveryLocation
        ? req.body.deliveryLocation
        : order.deliveryLocation;
    order.pickUpLocation = req.body.pickUpLocation
        ? req.body.pickUpLocation
        : order.pickUpLocation;
    order.driverGpsLocation = req.body.driverGpsLocation
        ? req.body.driverGpsLocation
        : order.driverGpsLocation;
    order.processingStatus = req.body.processingStatus
        ? req.body.processingStatus
        : order.processingStatus;
    order.deliveryAddress = req.body.deliveryAddress
        ? req.body.deliveryAddress
        : order.deliveryAddress;
    order.paymentType = req.body.paymentType
        ? req.body.paymentType
        : order.paymentType;
    order.paymentAmount = req.body.paymentAmount
        ? req.body.paymentAmount
        : order.paymentAmount;
    order.userRating = req.body.userRating
        ? req.body.userRating
        : order.userRating;
    order.deliveryRating = req.body.deliveryRating
        ? req.body.deliveryRating
        : order.deliveryRating;
    order.userReview = req.body.userReview
        ? req.body.userReview
        : order.userReview;
    order.otp = req.body.otp ? req.body.otp : order.otp;
    order.deliveryReview = req.body.deliveryReview
        ? req.body.deliveryReview
        : order.deliveryReview;
    order.shopReview = req.body.shopReview
        ? req.body.shopReview
        : order.shopReview;
    order.shopRating = req.body.shopRating
        ? req.body.shopRating
        : order.shopRating;
    order.anyIssue = req.body.anyIssue ? req.body.anyIssue : order.anyIssue;
    order.pickUpAddress = req.body.pickUpAddress
        ? req.body.pickUpAddress
        : order.pickUpAddress;
    order.deliveryInstructions = req.body.deliveryInstructions
        ? req.body.deliveryInstructions
        : order.deliveryInstructions;
    order.category = req.body.category ? req.body.category : order.category;
    order.deliveredDateTime = req.body.deliveredDateTime
        ? req.body.deliveredDateTime
        : order.deliveredDateTime;
    order.createdAt = req.body.createdAt ? req.body.createdAt : order.createdAt;
    order.updatedAt = req.body.updatedAt ? req.body.updatedAt : order.updatedAt;

    order
        .saveAsync()
        .then(savedorder => {
            const returnObj = {
                success: true,
                message: "order details updated successfully",
                data: savedorder
            };
            res.send(returnObj);
        })
        .error(errMessage => {
            const err = new APIError(
                `error in updating order ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({success: false, message: "not able to update order"});
            next(err);
        });
}

export default {
    load,
    get,
    create,
    cancel,
    checkPrice,
    update,
    getAll,
    verifyRide,
    deleteAll,
    fetchByDeliveryId,
    fetchLimitedOrders,
    fetchByUser
};
