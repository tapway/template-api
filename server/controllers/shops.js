import mongoose from "mongoose";
import Shops from "../models/shops";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Load shop
 * @set {req.shop}
 */
function load(req, res, next, shopId) {
  req.shopId = shopId;
  Shops.get(shopId)
    .then(shop => {
      req.shop = shop; // eslint-disable-line no-param-reassign
      return next();
    })
    .error(errMessage => {
      const err = new APIError(
        `error in loading shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in loading shop"
      });
      next(err);
    });
}

/**
 * Get shop
 * @returns {shop}
 */
function get(req, res) {
  return res.json({
    success: true,
    message: "Shop found",
    data: { shop: req.shop }
  });
}

/** Get All Shop
 * @param req
 * @param res
 * @param next
 * @returns {Array of Shops}
 */
function getAllShops(req, res, next) {
  const result = Shops.list(
    req.body.skip,
    req.body.limit,
    req.body.location,
    req.body.shopType
  );
  result
    .then(data => {
      return res.json({
        success: true,
        message: "Limited Shops Fetched",
        data
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in getting all shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in getting all shop"
      });
      next(err);
    });
}

/** Get All Shop By Filter
 * @param req
 * @param res
 * @param next
 * @returns {Array of Shops}
 */
function getNearbyShopsByFilter(req, res, next) {
  const result = Shops.listNearbyShopsWithFilter(
    req.body.skip,
    req.body.limit,
    req.body.location,
    req.body.shopType,
    req.body.query
  );
  result
    .then(data => {
      return res.json({
        success: true,
        message: "Limited Nearby Shops Fetched With Filter",
        data
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in getting all shops by filter ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in getting all shops by filter"
      });
      next(err);
    });
}

/** Create Shop
 * @param req
 * @param res
 * @param next
 * @returns {Saved Shop}
 */
function create(req, res, next) {
  const shop = new Shops({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    address: req.body.address,
    location: [
      parseFloat(req.body.location[0]),
      parseFloat(req.body.location[1])
    ],
    closingTime: req.body.closingTime,
    openingTime: req.body.openingTime,
    contact: req.body.contact,
    shopType: req.body.shopType
  });
  shop
    .saveAsync()
    .then(savedshop => {
      const returnObj = {
        success: true,
        message: "saved new shop",
        data: { savedshop }
      };
      res.json(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in saving shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in saving shop"
      });
      next(err);
    });
}

/** Update Shop
 * @param req
 * @param res
 * @param next
 * @returns {Update Shop}
 */
function update(req, res, next) {
  const shop = req.shop;
  shop.name = req.body.name ? req.body.name : shop.name;
  shop.address = req.body.address ? req.body.address : shop.address;
  shop.location = req.body.location ? req.body.location : shop.location;
  shop.openingTime = req.body.openingTime
    ? req.body.openingTime
    : shop.openingTime;
  shop.closingTime = req.body.closingTime
    ? req.body.closingTime
    : shop.closingTime;
  shop.contact = req.body.contact ? req.body.contact : shop.contact;
  shop.shopType = req.body.shopType ? req.body.shopType : shop.shopType;

  shop
    .saveAsync()
    .then(savedshop => {
      const returnObj = {
        success: true,
        message: "shop details updated successfully",
        data: savedshop
      };
      res.send(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating shop"
      });
      next(err);
    });
}

/** Delete Shop
 * @param req
 * @param res
 * @param next
 * @returns {Deleted Shop}
 */
function deleteShop(req, res, next) {
  findByIdAndRemoveAysnc(req.body.id)
    .then(removedshop => {
      const returnObj = {
        success: true,
        message: "shop removed",
        data: removedshop
      };
      res.send(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in deleting shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in deleting shop"
      });
      next(err);
    });
}

/** Remove All Shops
 * @param req
 * @param res
 * @param next
 * @returns {Deleted Count}
 */
function removeAll(req, res, next) {
  Shops.removeAsync()
    .then(deletedShops => res.json(deletedShops.deletedCount))
    .error(errMessage => {
      const err = new APIError(
        `error in removing shop ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing shop"
      });
      next(err);
    });
}

export default {
  load,
  get,
  create,
  update,
  deleteShop,
  removeAll,
  getAllShops,
  getNearbyShopsByFilter
};
