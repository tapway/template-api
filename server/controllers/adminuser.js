import jwt from "jsonwebtoken";
import Adminuser from "../models/adminuser";
import Orders from "../models/orders";
import mongoose from "mongoose";
const config = require("../../config/env");
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Load User from id
 * @Set {req.user}
 */
function load(req, res, next, id) {
  Adminuser.get(id)
    .then(user => {
      req.user = user;
      return next();
    })
    .error(errMessage => {
      const err = new APIError(
        `error in loading user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in loading user"
      });
      next(err);
    });
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  if (req.user) {
    return res.json({
      success: true,
      message: "User found",
      data: { user: req.user }
    });
  } else {
    return res.json({
      success: false,
      message: "User Not found"
    });
  }
}

/**
 * Create user
 * @returns {User}
 */
function create(req, res, next) {

  const user = new Adminuser({
    _id: new mongoose.Types.ObjectId(),
    fullName: req.body.userObj.fullName,
    firstName: req.body.userObj.firstName,
    lastName: req.body.userObj.lastName,
    address: req.body.userObj.address,
    gpsLocation: req.body.userObj.gpsLocation,
    savedAddresses: req.body.userObj.savedAddresses,
    phoneNumber: req.body.userObj.phoneNumber,
    email: req.body.userObj.email,
    password: req.body.userObj.password,
    deviceInfo: req.body.deviceInfo,
    profileImage: req.body.userObj.profileImage
  });
  user
    .saveAsync()
    .then(savedUser => {
      var i = "Teniola";
      var signOptions = {
        issuer: i,
        algorithm: "HS256"
      };
      const token = jwt.sign(
        { _doc: { _id: savedUser._id, type: "Adminuser" } },
        config.jwtSecret,
        signOptions
      );
      const returnObj = {
        success: true,
        message: "User Created",
        data: {
          jwtAccessToken: `JWT ${token}`,
          savedUser
        }
      };
      res.json(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in creating user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in creating user"
      });
      next(err);
    });
}

/**
 * Updates User
 * @returns {Updated User}
 */
function update(req, res, next) {
  const user = req.user;
  user.firstName = req.body.firstName ? req.body.firstName : user.firstName;
  user.fullName = req.body.fullName ? req.body.fullName : user.fullName;
  user.lastName = req.body.lastName ? req.body.lastName : user.lastName;
  user.address = req.body.address ? req.body.address : user.address;
  user.gpsLocation = req.body.gpsLocation
    ? req.body.gpsLocation
    : user.gpsLocation;
  user.savedAddresses = req.body.savedAddresses
    ? req.body.savedAddresses
    : user.savedAddresses;
  user.phoneNumber = req.body.phoneNumber
    ? req.body.phoneNumber
    : user.phoneNumber;
  user.email = req.body.email ? req.body.email : user.email;
  user.profileImage = req.body.profileImage
    ? req.body.profileImage
    : user.profileImage;

  user
    .saveAsync()
    .then(savedUser => {
      const returnObj = {
        success: true,
        message: "Adminuser details updated successfully",
        data: savedUser
      };
      res.send(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating end user"
      });
      next(err);
    });
}

/**
 * Add New Address
 * @returns {Updated User}
 */
function addNewAddress(req, res, next) {
  const user = req.user;
  user.savedAddresses = [...user.savedAddresses, req.body.newAddress];
  user
    .saveAsync()
    .then(savedUser => {
      const returnObj = {
        success: true,
        message: "Adminuser details updated successfully",
        data: savedUser
      };
      res.send(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in adding new address ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in adding new address"
      });
      next(err);
    });
}

/**
 * Updates Rating Of User
 * @returns {success}
 */
function updateRating(req, res, next) {
  Orders.aggregateAsync([
    {
      $match: {
        userrId: req.user._id,
        userRating: { $gt: 0 }
      }
    },
    {
      $group: {
        _id: "$userId",
        userRt: { $avg: "userRating" }
      }
    }
  ])
    .then(res => {
      return res.json({ success: false, message: "User Rating Not Updated" });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating rating ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating rating"
      });
      next(err);
    });
  return res.json({ success: true, message: "User Rating Updated" });
}

/**
 * Lists User with skip and limit
 * @returns {Array of users}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Adminuser.list({ limit, skip })
    .then(users => res.json(users))
    .error(errMessage => {
      const err = new APIError(
        `error in listing end users ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in listing end users"
      });
      next(err);
    });
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user
    .removeAsync()
    .then(deletedUser => res.json(deletedUser))
    .error(errMessage => {
      const err = new APIError(
        `error in removing end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing end user"
      });
      next(err);
    });
}

/**
 * Remove All the users
 * @returns {Deleted Count}
 */
function removeAll(req, res, next) {
  Adminuser.removeAsync()
    .then(deletedUser => res.json(deletedUser.deletedCount))
    .error(errMessage => {
      const err = new APIError(
        `error in removing all end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing all end user"
      });
      next(err);
    });
}

export default {
  load,
  get,
  create,
  updateRating,
  update,
  addNewAddress,
  removeAll,
  remove,
  list
};
