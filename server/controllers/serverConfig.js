import ServerConfig from "../models/serverConfig";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import _ from "lodash"; //eslint-disable-line

/**
 * Get server config
 * @returns {ServerConfig}
 */
function get(req, res, next) {
  ServerConfig.find((error, configData) => {
    //eslint-disable-line
    if (error) {
      const err = new APIError(
        `error while finding corresponding data  ${error}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      return next(err);
    }
    const configObj = {};
    _.map(configData, keyData => {
      configObj[keyData.key] = keyData.value;
    });
    res.send({
      success: true,
      message: "Server Config Created Successfully",
      data: configObj
    });
  });
}

/** Create new key-value pair in server config
 * @param req
 * @param res
 * @param next
 * @returns {Saved Config}
 */
function create(req, res, next) {
  const config = new ServerConfig({
    type: req.body.type,
    key: req.body.key,
    value: req.body.value
  });
  config
    .saveAsync()
    .then(savedConfig => {
      const returnObj = {
        success: true,
        message: "Server Config Created Successfully",
        data: savedConfig
      };
      res.send(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in saving server config ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in saving server config"
      });
      next(err);
    });
}

/** Update Config
 * @param req
 * @param res
 * @param next
 * @returns {Updated Config}
 */
function update(req, res, next) {
  const reqObj = Object.assign({}, req.body);
  const result = [];
  const keys = _.keys(reqObj);
  const values = _.values(reqObj);
  _.map(keys, (keyitem, index) => {
    ServerConfig.findOneAsync({ key: keyitem }).then(foundKey => {
      if (foundKey !== null) {
        ServerConfig.findOneAndUpdateAsync(
          { key: keyitem },
          { $set: { value: values[index] } },
          { new: true }
        )
          .then(updatedConfigObj => {
            if (updatedConfigObj) {
              result.push(updatedConfigObj);
              res.send(result);
            }
          })
          .error(e => {
            const err = new APIError(
              `error in updating user details while login ${e}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            next(err);
          });
      } else {
        const newConfig = new ServerConfig({
          type: typeof values[index],
          key: keyitem,
          value: values[index]
        });
        newConfig
          .saveAsync()
          .then(savedConfigObj => {
            result.push(savedConfigObj);
            if (result.length === keys.length) {
              res.send(result);
            }
          })
          .error(e => next(e));
      }
    });
  });
}

export default { get, create, update };
