var cloudinary = require("cloudinary").v2;
import serverConfig from "../models/serverConfig";
import formidable from "formidable";
import APIError from "../helpers/APIError";

/** Image Upload Function
 * @param req
 * @param res
 * @param next
 * @returns success message and image url
 */
export default function imagehandler(req, res, next) {
  const form = new formidable.IncomingForm();
  form.on("error", err => {
    console.error(err, "error in image upload"); //eslint-disable-line
  });

  serverConfig
    .findOne({ key: "cloudinaryConfig" })
    .execAsync()
    .then(cloudinaryConfig => {
      if (cloudinaryConfig) {

        cloudinary.config({
          cloud_name: cloudinaryConfig.value.cloud_name,
          api_key: cloudinaryConfig.value.api_key,
          api_secret: cloudinaryConfig.value.api_secret
        });

        form.parse(req, (err, fields, files) => {
          const imgpath = files.image;
          cloudinary.uploader.upload(
            imgpath.path,
            { quality: "auto:eco" },
            (error, results) => {
              if (results) {
                const returnObj = {
                  success: true,
                  message: "Image updated",
                  data: { url: results.secure_url }
                };
                return res.json(returnObj);
              } else {
                const returnObj = {
                  success: false,
                  message: "Image not updated"
                };
                return res.json(returnObj);
              }
            }
          );
        });
      }
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in fetching cloudinary config ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      throw err;
    });
}
