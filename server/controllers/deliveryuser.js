import jwt from "jsonwebtoken";
import deliverySchema from "../models/deliveryuser";
import mongoose from "mongoose";
const config = require("../../config/env");
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Load User from id
 * @Set {req.user}
 */
function load(req, res, next, id) {
  deliverySchema
    .get(id)
    .then(user => {
      req.user = user;
      return next();
    })
    .error(errMessage => {
      const err = new APIError(
        `error in loading delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in loading delivery user"
      });
      next(err);
    });
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  return res.json({
    success: true,
    message: "Delivery User found",
    data: { deliveryUser: req.user }
  });
}

/**
 * Create user
 * @returns {User}
 */
function create(req, res, next) {
  const deliveryGuy = new deliverySchema({
    _id: new mongoose.Types.ObjectId(),
    fullName: req.body.userobj.fullName,
    dob: req.body.userobj.dob,
    gender: req.body.userobj.gender,
    firstName: req.body.userobj.firstName,
    lastName: req.body.userobj.lastName,
    address: req.body.userobj.address,
    deviceInfo: req.body.deviceInfo,
    gpsLocation: req.body.userobj.GpsLocation,
    savedAddresses: req.body.userobj.savedAddresses,
    isLoggedIn: req.body.userobj.isLoggedIn,
    phoneNumber: req.body.userobj.phoneNumber,
    email: req.body.userobj.email,
    verificationDetails: req.body.userobj.verificationDetails,
    profileImage: req.body.userobj.profileImage,
    verificationImage: req.body.userobj.verificationImage,
    vehicleVerificationImage: req.body.userobj.vehicleVerificationImage,
    vehicleDetails: req.body.userobj.vehicleDetails,
    bankDetails: req.body.userobj.bankDetails
  });
  deliveryGuy
    .saveAsync()
    .then(user => {
      var i = "Geekyants";
      var signOptions = {
        issuer: i,
        algorithm: "HS256"
      };
      const token = jwt.sign(
        { _doc: { _id: user._id, type: "deliveryUser" } },
        config.jwtSecret,
        signOptions
      );
      const returnObj = {
        success: true,
        message: "Delivery user created",
        data: { jwtAccessToken: `JWT ${token}`, user }
      };
      res.json(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in saving delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in saving delivery user"
      });
      next(err);
    });
}

/**
 * Check if user exists
 * @returns {succes:true,User}
 */
function check(req, res, next) {
  deliverySchema
    .findOne({ phoneNumber: req.body.phoneNumber })
    .execAsync()
    .then(user => {
      if (user) {
        const returnObj = {
          success: true,
          message: "Delivery user found",
          data: { user }
        };
        return res.json(returnObj);
      } else {
        const returnObj = {
          success: false,
          message: "Delivery user not found",
          data: { user }
        };
        return res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in checking delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in checking delivery user"
      });
      next(err);
    });
}

/**
 * Lists User with skip and limit
 * @returns {Array of users}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  deliverySchema
    .list({ limit, skip })
    .then(users => res.json(users))
    .error(errMessage => {
      const err = new APIError(
        `error in listing delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in listing delivery user"
      });
      next(err);
    });
}

/**
 * Remove All the users
 * @returns {Deleted Count}
 */
function removeAll(req, res, next) {
  deliverySchema
    .removeAsync()
    .then(deletedUser => res.json(deletedUser.deletedCount))
    .error(errMessage => {
      const err = new APIError(
        `error in removing all delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing all delivery user"
      });
      next(err);
    });
}

/**
 * Updates User
 * @returns {Updated User}
 */
function update(req, res, next) {
  let guy = req.user;
  const bankDetails = {
    bankName: req.body.userobj.bankDetails.bankName
      ? req.body.userobj.bankDetails.bankName
      : guy.bankDetails.bankName,
    accountNumber: req.body.userobj.bankDetails.accountNumber
      ? req.body.userobj.bankDetails.accountNumber
      : guy.bankDetails.accountNumber,
    IFSC_Code: req.body.userobj.bankDetails.IFSC_Code
      ? req.body.userobj.bankDetails.IFSC_Code
      : guy.bankDetails.IFSC_Code,
    accountName: req.body.userobj.bankDetails.accountName
      ? req.body.userobj.bankDetails.accountName
      : guy.bankDetails.accountName
  };
  const vehicleDetails = {
    vehicleVerificationModal: req.body.userobj.vehicleDetails
      .vehicleVerificationModal
      ? req.body.userobj.vehicleDetails.vehicleVerificationModal
      : guy.vehicleDetails.vehicleVerificationModal,
    vehicleRegistrationNumber: req.body.userobj.vehicleDetails
      .vehicleRegistrationNumber
      ? req.body.userobj.vehicleDetails.vehicleRegistrationNumber
      : guy.vehicleDetails.vehicleRegistrationNumber,
    vehicleInsurancePolicy: req.body.userobj.vehicleDetails
      .vehicleInsurancePolicy
      ? req.body.userobj.vehicleDetails.vehicleInsurancePolicy
      : guy.vehicleDetails.vehicleInsurancePolicy,
    vehicleDate: req.body.userobj.vehicleDetails.vehicleDate
      ? req.body.userobj.vehicleDetails.vehicleDate
      : guy.vehicleDetails.vehicleDate,
    vehicleNumber: req.body.userobj.vehicleDetails.vehicleNumber
      ? req.body.userobj.vehicleDetails.vehicleNumber
      : guy.vehicleDetails.vehicleNumber
  };
  const verificationDetails = {
    address: req.body.userobj.verificationDetails.address
      ? req.body.userobj.verificationDetails.address
      : guy.verificationDetails.address,
    licenceNumber: req.body.userobj.verificationDetails.licenceNumber
      ? req.body.userobj.verificationDetails.licenceNumber
      : guy.verificationDetails.licenceNumber,
    cardHolder: req.body.userobj.verificationDetails.cardHolder
      ? req.body.userobj.verificationDetails.cardHolder
      : guy.verificationDetails.cardHolder,
    dateOfIssue: req.body.userobj.verificationDetails.dateOfIssue
      ? req.body.userobj.verificationDetails.dateOfIssue
      : guy.verificationDetails.dateOfIssue,
    validity: req.body.userobj.verificationDetails.validity
      ? req.body.userobj.verificationDetails.validity
      : guy.verificationDetails.validity
  };
  guy.fullName = req.body.userobj.fullName
    ? req.body.userobj.fullName
    : guy.fullName;
  guy.dob = req.body.userobj.dob ? req.body.userobj.dob : guy.dob;
  guy.gender = req.body.userobj.gender ? req.body.userobj.gender : guy.gender;
  guy.address = req.body.userobj.address
    ? req.body.userobj.address
    : guy.address;
  guy.isLoggedIn = req.body.userobj.isLoggedIn
    ? req.body.userobj.isLoggedIn
    : guy.isLoggedIn;
  guy.gpsLocation = req.body.userobj.gpsLocation
    ? req.body.userobj.gpsLocation
    : guy.gpsLocation;
  guy.savedAddresses = req.body.userobj.savedAddresses
    ? req.body.userobj.savedAddresses
    : guy.savedAddresses;
  guy.phoneNumber = req.body.userobj.phoneNumber
    ? req.body.userobj.phoneNumber
    : guy.phoneNumber;
  guy.email = req.body.userobj.email ? req.body.userobj.email : guy.email;
  guy.profileImage = req.body.userobj.profileImage
    ? req.body.userobj.profileImage
    : guy.profileImage;
  guy.verificationImage = req.body.userobj.verificationImage
    ? req.body.userobj.verificationImage
    : guy.verificationImage;
  guy.vehicleType = req.body.userobj.vehicleType
    ? req.body.userobj.vehicleType
    : guy.vehicleType;
  guy.vehicleNumber = req.body.userobj.vehicleNumber
    ? req.body.userobj.vehicleNumber
    : guy.vehicleNumber;
  guy.bankDetails = bankDetails;
  guy.vehicleDetails = vehicleDetails;
  guy.verificationDetails = verificationDetails;
  guy.address = req.body.userobj.address
    ? req.body.userobj.address
    : guy.address;

  guy
    .saveAsync()
    .then(savedUser => {
      return res.json({
        success: true,
        message: "Delivery user updated",
        data: savedUser
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating delivery user"
      });
      next(err);
    });
}

/**
 * Lists User with skip and limit
 * @returns {Array of users}
 */
function setAvailable(req, res, next) {
  deliverySchema
    .findOneAndUpdateAsync(
      { _id: req.user._id },
      { $set: { isAvailable: req.body.set } },
      { new: true }
    )
    .then(updatedUser => {
      const returnObj = {
        success: true,
        message: "user successfully updated"
      };
      res.json(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in setting available delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in setting available delivery user"
      });
      next(err);
    });
}

/**
 * Set Trip Status
 * @returns {success}
 */
function setTripStatus(req, res, next) {
  deliverySchema
    .findOneAndUpdateAsync(
      { _id: req.user._id },
      { $set: { onTrip: req.body.set } },
      { new: true }
    )
    .then(updatedUser => {
      const returnObj = {
        success: true,
        message: "User successfully updated"
      };
      res.json(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in setting trip status ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in setting trip status"
      });
      next(err);
    });
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user
    .removeAsync()
    .then(deletedUser => res.json(deletedUser))
    .error(errMessage => {
      const err = new APIError(
        `error in removing user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing user"
      });
      next(err);
    });
}

export default {
  load,
  create,
  list,
  remove,
  update,
  get,
  check,
  removeAll,
  setAvailable,
  setTripStatus
};
