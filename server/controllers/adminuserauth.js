import jwt from "jsonwebtoken";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import sendMessage from "../service/sendMessage";
import AdminUserSchema from "../models/adminuser";
const config = require("../../config/env");
const bcrypt = require('bcrypt');
/**
 * Will return random number.
 * @returns {otp}
 */
function getRandomNumber() {
  const otp = Math.floor(100000 + Math.random() * 900000);
  return otp;
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
function login(req, res, next) {
  const data = req.body;
  let UserData, UserDetails;


  AdminUserSchema.findOne({
    email: data.email
  })
      .then(user => {
        if (!user) {
          const error = new Error('An User with this email could not be found.');
          error.statusCode = 801;
          throw error;
        }
        UserData = user;
        UserDetails = user.toJSON();
        return bcrypt.compare(data.password, user.password);
      })
      .then(isEqual => {
        if (!isEqual) {
          const error = new Error('Wrong password!');
          error.statusCode = 901;
          throw error;
        }
        const token = jwt.sign({
              username: UserData.username,
              userId: UserData._id.toString()
            },
            'somesupersecretsecret', {
              expiresIn: '30d'
            }
        );
        res.status(200).json({
          message: "Auth successful, you are now logged",
          token: token,
          data: {UserDetails}

        });
      })
      .catch(err => {
        console.log(err);
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      });
}

/**
 * Returns jwt token if valid otp is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function resendOtp(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  AdminUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        sendMessage(req.body.phoneNumber, user.otp);
        const returnObj = {
          success: true,
          message: "Otp send"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in resending otp ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in resending otp"
      });
      next(err);
    });
}

/**
 * Returns true if user exists
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function checkUserExistance(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  AdminUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User does not exist"
        };
        res.json(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: "User exists already"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in checking existance of end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in checking existance of end user"
      });
      next(err);
    });
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
function otpVerify(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber,
    otp: req.body.otp
  };
  AdminUserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        var i = "Tapway";
        var signOptions = {
          issuer: i,
          algorithm: "HS256"
        };
        const token = jwt.sign(
          { _doc: { _id: user._id, type: "endUser" } },
          config.jwtSecret,
          signOptions
        );
        AdminUserSchema.findOneAndUpdateAsync(
          { _id: user._id },
          {
            $set: {
              isVerified: true,
              deviceInfo: req.body.deviceInfo ? req.body.deviceInfo : null
            }
          },
          { new: true }
        )
          .then(updatedUser => {
            const returnObj = {
              success: true,
              message: "User successfully logged in",
              data: {
                jwtAccessToken: `JWT ${token}`,
                user: updatedUser
              }
            };
            res.json(returnObj);
          })
          .error(errMessage => {
            const err = new APIError(
              `error in otp verification of end user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
              success: false,
              message: "error in otp verification of end user"
            });
            next(err);
          });
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in finding user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in finding user"
      });
      next(err);
    });
}

/** This is a protected route. Change login status to false and send success message.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function logout(req, res, next) {
  const userObj = req.body.user;
  if (userObj === undefined || userObj === null) {
  }
  AdminUserSchema.findOneAndUpdate(
    { _id: userObj.id },
    { $set: userObj },
    { new: true },
    (err, userDoc) => {
      if (err) {
        const error = new APIError(
          "error while updateing login status",
          httpStatus.INTERNAL_SERVER_ERROR
        );
        next(error);
      }
      if (userDoc) {
        const returnObj = {
          success: true,
          message: "User logout successfully"
        };
        res.json(returnObj);
      } else {
        const error = new APIError("user not found", httpStatus.NOT_FOUND);
        next(error);
      }
    }
  );
}

export default { login, resendOtp, logout, otpVerify, checkUserExistance };
