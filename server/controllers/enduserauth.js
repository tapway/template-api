import jwt from "jsonwebtoken";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import sendMessage from "../service/sendMessage";
import EnduserSchema from "../models/enduser";
const config = require("../../config/env");

/**
 * Will return random number.
 * @returns {otp}
 */
function getRandomNumber() {
  const otp = Math.floor(100000 + Math.random() * 900000);
  return otp;
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
function login(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  EnduserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "user not found"
        };
        res.json(returnObj);
      } else {
        const token = getRandomNumber();
        sendMessage(req.body.phoneNumber, token);
        EnduserSchema.findOneAndUpdateAsync(
          { _id: user._id },
          { $set: { otp: token } },
          { new: true }
        )
          .then(() => {
            const returnObj = {
              success: true,
              message: "user found",
              data: {}
            };
            res.json(returnObj);
          })
          .error(errMessage => {
            const err = new APIError(
              `error in logging end user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
              success: false,
              message: "error in logging end user"
            });
            next(err);
          });
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in finding end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in finding end user"
      });
      next(err);
    });
}

/**
 * Returns jwt token if valid otp is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function resendOtp(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  EnduserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        sendMessage(req.body.phoneNumber, user.otp);
        const returnObj = {
          success: true,
          message: "Otp send"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in resending otp ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in resending otp"
      });
      next(err);
    });
}

/**
 * Returns true if user exists
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function checkUserExistance(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  EnduserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User does not exist"
        };
        res.json(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: "User exists already"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in checking existance of end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in checking existance of end user"
      });
      next(err);
    });
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
function otpVerify(req, res, next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber,
    otp: req.body.otp
  };
  EnduserSchema.findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "User not found"
        };
        res.json(returnObj);
      } else {
        var i = "Geekyants";
        var signOptions = {
          issuer: i,
          algorithm: "HS256"
        };
        const token = jwt.sign(
          { _doc: { _id: user._id, type: "endUser" } },
          config.jwtSecret,
          signOptions
        );
        EnduserSchema.findOneAndUpdateAsync(
          { _id: user._id },
          {
            $set: {
              isVerified: true,
              deviceInfo: req.body.deviceInfo ? req.body.deviceInfo : null
            }
          },
          { new: true }
        )
          .then(updatedUser => {
            const returnObj = {
              success: true,
              message: "User successfully logged in",
              data: {
                jwtAccessToken: `JWT ${token}`,
                user: updatedUser
              }
            };
            res.json(returnObj);
          })
          .error(errMessage => {
            const err = new APIError(
              `error in otp verification of end user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            res.json({
              success: false,
              message: "error in otp verification of end user"
            });
            next(err);
          });
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in finding user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in finding user"
      });
      next(err);
    });
}

/** This is a protected route. Change login status to false and send success message.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function logout(req, res, next) {
  const userObj = req.body.user;
  if (userObj === undefined || userObj === null) {
  }
  EnduserSchema.findOneAndUpdate(
    { _id: userObj.id },
    { $set: userObj },
    { new: true },
    (err, userDoc) => {
      if (err) {
        const error = new APIError(
          "error while updateing login status",
          httpStatus.INTERNAL_SERVER_ERROR
        );
        next(error);
      }
      if (userDoc) {
        const returnObj = {
          success: true,
          message: "User logout successfully"
        };
        res.json(returnObj);
      } else {
        const error = new APIError("user not found", httpStatus.NOT_FOUND);
        next(error);
      }
    }
  );
}

export default { login, resendOtp, logout, otpVerify, checkUserExistance };
