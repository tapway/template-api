import Orders from "../models/orders";
const moment = require("moment");
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Fetch Earning Through Driver Id
 * @return {Earnings}
 */
function fetchEarnings(req, res, next) {
  return Orders.find({ deliveryId: req.user._id })
    .execAsync()
    .then(result => {
      let todayEarn = 0;
      let yesterdayEarn = 0;
      let weekEarn = 0;
      let monthEarn = 0;
      let walletBalance = 0;
      for (var order in result) {
        if (
          moment().diff(moment(result[order].deliveredDateTime), "days") === 0
        ) {
          todayEarn += result[order].paymentAmount;
        }
        if (
          moment().diff(moment(result[order].deliveredDateTime), "days") === 1
        ) {
          yesterdayEarn += result[order].paymentAmount;
        }
        if (
          moment().diff(moment(result[order].deliveredDateTime), "days") >= 0 &&
          moment().diff(moment(result[order].deliveredDateTime), "days") <= 7
        ) {
          weekEarn += result[order].paymentAmount;
        }
        if (
          moment().diff(moment(result[order].deliveredDateTime), "days") >= 0 &&
          moment().diff(moment(result[order].deliveredDateTime), "days") <= 31
        ) {
          monthEarn += result[order].paymentAmount;
        }
        if (result[order].paymentType !== "cash") {
          walletBalance += result[order].paymentAmount;
        }
      }

      todayEarn = Math.round(todayEarn * 100) / 100;
      yesterdayEarn = Math.round(yesterdayEarn * 100) / 100;
      weekEarn = Math.round(weekEarn * 100) / 100;
      monthEarn = Math.round(monthEarn * 100) / 100;
      walletBalance = Math.round(walletBalance * 100) / 100;

      return res.json({
        success: true,
        message: "Found earnings",
        data: { todayEarn, yesterdayEarn, weekEarn, monthEarn, walletBalance }
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in fetching earnings ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in fetching earnings"
      });
      next(err);
    });
}

export default { fetchEarnings };
