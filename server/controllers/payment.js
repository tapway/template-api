import Payment from "../models/payment";
import mongoose from "mongoose";
import config from "../../config/env";
var request = require("request");
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Load Payment
 * @set {req.payment}
 */
function load(req, res, next, id) {
  Payment.get(id)
    .then(payment => {
      req.payment = payment; // eslint-disable-line no-param-reassign
      return next();
    })
    .error(errMessage => {
      const err = new APIError(
        `error in loading payment ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in loading payment"
      });
      next(err);
    });
}

/**
 * Get Payment
 * @returns {payment}
 */
function get(req, res) {
  return res.json({
    success: true,
    message: "Payment found",
    data: { payment: req.payment }
  });
}

/** Create Payment
 * @param req
 * @param res
 * @param next
 * @returns {Saved Payment}
 */
function create(req, res, next) {
  const payment = new Payment({
    _id: new mongoose.Types.ObjectId(),
    paymentId: req.body.paymentId,
    amount: req.body.amount,
    orderId: req.body.orderId,
    verified: req.body.verified,
    invoiceId: req.body.invoiceId,
    type: req.body.type
  });
  payment
    .saveAsync()
    .then(payment => {
      const returnObj = {
        success: true,
        message: "Payment created",
        data: { payment: payment }
      };
      res.json(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in saving payment ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in saving payment"
      });
      next(err);
    });
}

/** Check Payment
 * @param req
 * @param res
 * @param next
 * @returns {success,payment}
 */
function check(req, res, next) {
  Payment.findOne({ paymentId: req.body.paymentId })
    .execAsync()
    .then(payment => {
      if (payment) {
        const returnObj = {
          success: true,
          message: "Payment found",
          data: { payment }
        };
        return res.json(returnObj);
      } else {
        const returnObj = {
          success: false,
          message: "Payment not found",
          data: { payment }
        };
        return res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in checking payment ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in checking payment"
      });
      next(err);
    });
}

/** Capture Payment
 * @param req
 * @param res
 * @param next
 * @returns {success}
 */
function captureRazorPayPayment(req, res, next) {
  request(
    {
      method: "POST",
      url:
        "https://" +
        config.RAZOR_PAY_KEY +
        ":" +
        config.RAZOR_PAY_SECRET +
        "@api.razorpay.com/v1/payments/" +
        req.body.paymentId +
        "/capture",
      form: { amount: req.body.amount }
    },
    function(error, response, body) {
      if (error) {
        const err = new APIError(
          `error in capture payment ${error}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        res.json({
          success: false,
          message: "error in capture payment"
        });
        next(err);
      } else {
        res.json({ response, body });
      }
    }
  );
}

/** Capture Paypal Payment
 * @param req
 * @param res
 * @param next
 * @returns {success}
 */
function capturePaypalPayment(req, res, next) {
  request(
    {
      method: "POST",
      url:
        "https://api.sandbox.paypal.com/v2/payments/authorizations/" +
        req.body.authorizationId +
        "/capture",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer Access-Token",
        "PayPal-Request-Id": "123e4567-e89b-12d3-a456-426655440010"
      },
      body: JSON.stringify({
        amount: {
          value: req.body.amount,
          currency_code: req.body.currency_code
        },
        invoice_id: req.body.invoice_id,
        final_capture: true
      })
    },
    function(error, response, body) {
      if (error) {
        const err = new APIError(
          `error in capture payment ${error}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        res.json({
          success: false,
          message: "error in capture payment"
        });
        next(err);
      } else {
        res.json({ response, body });
      }
    }
  );
}

/** Remove All Payment
 * @param req
 * @param res
 * @param next
 * @returns {Deleted Count}
 */
function removeAll(req, res, next) {
  Payment.removeAsync()
    .then(deletedUser => res.json(deletedUser.deletedCount))
    .error(errMessage => {
      const err = new APIError(
        `error in removing all payment ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing all payment"
      });
      next(err);
    });
}

/** Update Payment
 * @param req
 * @param res
 * @param next
 * @returns {Update Payment}
 */
function update(req, res, next) {
  const payment = req.payment;
  payment.paymentId = req.body.paymentId
    ? req.body.paymentId
    : payment.paymentId;
  payment.amount = req.body.amount ? req.body.amount : payment.amount;
  payment.orderId = req.body.orderId ? req.body.orderId : payment.orderId;
  payment.verified = req.body.verified ? req.body.verified : payment.verified;
  payment
    .saveAsync()
    .then(savedPayment => {
      res.json({
        success: true,
        message: "payment updated",
        data: savedPayment
      });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating payment ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating payment"
      });
      next(err);
    });
}

/**
 * Delete Payment.
 * @returns {Payment}
 */
function remove(req, res, next) {
  const user = req.payment;
  user
    .removeAsync()
    .then(deletedUser => res.json(deletedUser))
    .error(errMessage => {
      const err = new APIError(
        `error in removing payment ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in removing payment"
      });
      next(err);
    });
}

export default {
  load,
  create,
  remove,
  update,
  captureRazorPayPayment,
  capturePaypalPayment,
  get,
  check,
  removeAll
};
