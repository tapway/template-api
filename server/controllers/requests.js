import mongoose from "mongoose";
import Requests from "../models/requests";
import Categories from "../models/categories";
import Orderstatus from "../models/orderstatus";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

function load(req, res, next, requestid) {
  req.requestid = requestid;
  Requests.get(requestid)
    .then(user => {
      req.requestVar = user;
      return next();
    })
    .error(errMessage => {
      const err = new APIError(
        `error in loading request ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in loading request"
      });
      next(err);
    });
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  return res.json({
    success: true,
    message: "Request found",
    data: { request: req.requestVar }
  });
}

function checkPrice(req, res, next) {

  Requests.find({
    pickUpLocation: req.body.pickUpLocation,
    deliveryLocation: req.body.deliveryLocation,
    packageSize: req.body.packageSize,
    itemQuantity: req.body.itemQuantity
  }).exec((err, requestList) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    return res.status(200).json({
      success: true,
      message: "saved",
      data: { requestList }

    });
  });

}

/** Create Request
 * @param req
 * @param res
 * @param next
 * @returns {Saved Request}
 */

function create(req, res, next) {
  const orderstatus = new Orderstatus({
    _id: new mongoose.Types.ObjectId(),
    status: "Pending"
  });
  const category = new Categories({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.category,
    img: req.body.categoryimg
  });
  orderstatus
    .saveAsync()
    .then(savedstatus => {
      category
        .saveAsync()
        .then(savedvategory => {
          const request = new Requests({
            userId: req.body.newreq.userId,
            category: category._id,
            processingStatus: orderstatus._id,
            deliveryInstructions: req.body.newreq.deliveryInstructions,
            paymentMode: req.body.newreq.paymentMode,
            paymentAmount: req.body.newreq.paymentAmount,
            endUserGpsLocation: req.body.newreq.endUserGpsLocation,
            deliveryLocation: req.body.newreq.deliveryLocation,
            pickUpLocation: req.body.newreq.pickUpLocation,
            deliveryAddress: req.body.newreq.deliveryAddress,
            pickUpAddress: req.body.newreq.pickUpAddress,
            itemQuantity: req.body.newreq.itemQuantity,
            packageSize: req.body.newreq.packageSize
          });
          request
            .saveAsync()
            .then(savedreq => {
              const returnObj = {
                success: true,
                message: "saved",
                data: { savedreq }
              };
              res.json(returnObj);
            })
            .error(errMessage => {
              const err = new APIError(
                `error in saving request ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
              );
              res.json({
                success: false,
                message: "error in saving request"
              });
              next(err);
            });
        })
        .error(errMessage => {
          const err = new APIError(
            `error in saving category ${errMessage}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          res.json({
            success: false,
            message: "error in saving category"
          });
          next(err);
        });
    })
    .error(errMessage => {
      const err = new APIError(
        `error in saving status ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in saving status"
      });
      next(err);
    });
}

/** Update Request
 * @param req
 * @param res
 * @param next
 * @returns {Updated Request}
 */
function update(req, res,next) {
  const request = req.requestVar;
  request.userId = req.body.userId ? req.body.userId : request.userId;
  request.category = req.body.category ? req.body.category : request.category;
  request.deliveryInstructions = req.body.deliveryInstructions
    ? req.body.deliveryInstructions
    : request.deliveryInstructions;
  request.processingStatus = req.body.processingStatus
    ? req.body.processingStatus
    : request.processingStatus;
  request.paymentMode = req.body.paymentMode
    ? req.body.paymentMode
    : request.paymentMode;
  request.paymentAmount = req.body.paymentAmount
    ? req.body.paymentAmount
    : request.paymentAmount;
  request.endUserGpsLocation = req.body.endUserGpsLocation
    ? req.body.endUserGpsLocation
    : request.endUserGpsLocation;
  request.deliveryLocation = req.body.deliveryLocation
    ? req.body.deliveryLocation
    : request.deliveryLocation;
  request.pickUpLocation = req.body.pickUpLocation
    ? req.body.pickUpLocation
    : request.pickUpLocation;

  request
    .saveAsync()
    .then(savedorder => {
      const returnObj = {
        success: true,
        message: "request details updated successfully",
        data: savedorder
      };
      res.send(returnObj);
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating request ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating request"
      });
      next(err);
    });
}

/** Update Status
 * @param req
 * @param res
 * @param next
 * @returns {Saved Order Status}
 */
function updateStatus(req, res,next) {
  Orderstatus.findByIdAndUpdate(
    req.requestid,
    { $set: { status: req.body.newstatus } },
    { new: true }
  )
    .execAsync()
    .then(user => {
      if (user) {
        const returnObj = {
          success: true,
          message: "updated",
          data: { user }
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in updating status ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in updating status"
      });
      next(err);
    });
}

export default { load, get, create, checkPrice, updateStatus, update };
