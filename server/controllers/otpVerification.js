import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import otpVerificationSchema from "../models/otpVerification";
import deliveryCtrl from "../controllers/deliveryuser";
import userCtrl from "../controllers/enduser";
import sendMessage from "../service/sendMessage";

/**
 * Will return random number.
 * @returns {otp}
 */
function getRandomNumber() {
  const otp = Math.floor(1000 + Math.random() * 9000);
  return otp;
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function register(req, res, next) {
  const token = getRandomNumber();
  sendMessage(req.body.phoneNumber, token);
  const newUser = new otpVerificationSchema({
    otp: token,
    phoneNumber: req.body.phoneNumber
  });
  newUser
    .saveAsync()
    .then(savedGuy => {
      if (!savedGuy) {
        const returnObj = {
          success: false,
          message: "User not created"
        };
        res.json(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: "User created"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in creating otp verification ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "otp verification not created"
      });
      next(err);
    });
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function otpVerifyDeliveryUser(req, res, next) {
  const userObject = {
    phoneNumber: req.body.phoneNumber,
    otp: req.body.otp
  };
  otpVerificationSchema
    .findOneAsync(userObject)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "Not Verified"
        };
        res.json(returnObj);
      } else {
        deliveryCtrl.create(req, res, next);
        otpVerificationSchema
          .removeAsync(user)
          .error(e => next(e));
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in verifing otp for delivery user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in verifing otp for delivery user"
      });
      next(err);
    });
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function otpVerifyEndUser(req, res, next) {
  const userObject = {
    phoneNumber: req.body.phoneNumber,
    otp: req.body.otp
  };
  otpVerificationSchema
    .findOneAsync(userObject)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "Not Verified"
        };
        res.json(returnObj);
      } else {
        userCtrl.create(req, res, next);
        otpVerificationSchema
          .removeAsync(user)
          .error(e => next(e));
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in verifing otp for end user ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in verifing otp for end user"
      });
      next(err);
    });
}

/** This is a protected route.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */
//
function resendOtp(req, res,next) {
  const userObj = {
    phoneNumber: req.body.phoneNumber
  };
  otpVerificationSchema
    .findOneAsync(userObj)
    .then(user => {
      if (!user) {
        const returnObj = {
          success: false,
          message: "user not found"
        };
        res.json(returnObj);
      } else {
        sendMessage(req.body.phoneNumber, user.otp);
        const returnObj = {
          success: true,
          message: "otp send"
        };
        res.json(returnObj);
      }
    })
    .error(errMessage => {
      const err = new APIError(
        `error in resending otp ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      res.json({
        success: false,
        message: "error in resending otp"
      });
      next(err);
    });
}

export default { otpVerifyDeliveryUser, otpVerifyEndUser, resendOtp, register };
