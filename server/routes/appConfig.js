import express from "express";
import appConfigCtrl from "../controllers/appConfig";

const router = express.Router();

// /** GET /api/config/appConfig - Returns mobileApp config */
router.route("/").get(appConfigCtrl.getConfig);

router.route("/create").post(appConfigCtrl.createConfig);

router.route("/update").put(appConfigCtrl.updateConfig);

export default router;
