import express from 'express';
import paymentCtrl from '../controllers/payment';
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';

const router = express.Router();

router.route('/').get(paymentCtrl.get);

router.route('/create').post(paymentCtrl.create);

router.route('/check').post(paymentCtrl.check);

router.route('/captureRazorPayPayment').post(paymentCtrl.captureRazorPayPayment);

router.route('/capturePaypalPayment').post(paymentCtrl.capturePaypalPayment);

router.route('/removeAll').delete(paymentCtrl.removeAll);

router.route('/:id').get(paymentCtrl.get);

router.route('/remove/:id').delete(paymentCtrl.remove)

router.route('/update/:id').put(paymentCtrl.update);

router.param('id', paymentCtrl.load);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });

export default router;