import express from "express";
import earningCtrl from "../controllers/earnings";
import httpStatus from 'http-status';
import passport from 'passport';
import config from '../../config/env';
import APIError from '../helpers/APIError';

const router = express.Router();

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });

router.route("/fetchEarnings").get(earningCtrl.fetchEarnings);

export default router;