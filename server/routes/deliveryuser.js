import express from "express";
import deliveryCtrl from "../controllers/deliveryuser";
import imagehandler from "../controllers/imagehandler";
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';

const router = express.Router();

router.route("/create").post(deliveryCtrl.create);

router.route("/updateImage").put(imagehandler);

/**
 * Routes For Admin
 */

router.route("/").get(deliveryCtrl.get);

router.route("/remove/:id").delete(deliveryCtrl.remove);

router.route("/removeAll").delete(deliveryCtrl.removeAll);

router.route("/check").post(deliveryCtrl.check);

router.route("/list").get(deliveryCtrl.list);

router.param("id", deliveryCtrl.load);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });

router.route("/setAvailable").put(deliveryCtrl.setAvailable);

router.route("/setTripStatus").put(deliveryCtrl.setTripStatus);

router.route("/update").put(deliveryCtrl.update);

export default router;
