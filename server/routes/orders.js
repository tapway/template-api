import express from "express";
import userCtrl from "../controllers/orders";
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';

const router = express.Router();


router.route("/").get(userCtrl.get);

router.route("/create").post(userCtrl.create);

router.route("/cancel").post(userCtrl.cancel);

router.route("/:orderId").get(userCtrl.get);

router.route("/update/:orderId").put(userCtrl.update);

router.route("/all").get(userCtrl.getAll);

router.route("/verifyRide").post(userCtrl.verifyRide);

router.route("/fetch/:deliveryId").get(userCtrl.fetchByDeliveryId);

router.route("/deleteAll").delete(userCtrl.deleteAll);

router.route("/fetchUserOrders/:userId").get(userCtrl.fetchByUser);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });

router.route("/fetchLimitedOrders").post(userCtrl.fetchLimitedOrders);

router.param("orderId", userCtrl.load);

export default router;
