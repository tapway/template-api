import express from 'express';
import endUserRoutes from './enduser';
import adminUserRoutes from './adminuser';
import requests from './requests';
import orders from './orders';
import deliveryUser from './deliveryuser';
import auth from './auth';
import earnings from "./earnings";
import endUserAuth from './enduserauth';
import adminUserAuth from './adminuserauth';
import items from './items';
import shops from './shops';
import otpVerification from './otpVerification';
import serverConfig from './serverConfig';
import appConfig from "./appConfig";

import payment from './payment';

const router = express.Router();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.get('/', (req, res) =>
  res.send('OK test')
);

router.use('/endUser', endUserRoutes);
router.use('/adminUser', adminUserRoutes);

router.use('/orders', orders);

router.use('/deliveryUser',deliveryUser);

router.use('/auth',auth);
router.use('/endUserAuth',endUserAuth);
router.use('/adminUserAuth',adminUserAuth);




router.use('/earnings',earnings);

router.use('/shops',shops);

router.use('/requests', requests);

router.use('/serverConfig', serverConfig);
router.use("/appConfig", appConfig);


router.use('/otpVerification', otpVerification);

router.use('/dashboard/items',items);

router.use('/payment',payment);

export default router;
