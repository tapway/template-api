import express from 'express';
import userCtrl from '../controllers/requests';
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';


const router = express.Router();

router.route('/').get(userCtrl.get);

router.route('/create').post(userCtrl.create);

router.route("/check_price").post(userCtrl.checkPrice);

router.route('/update/:requestId').put(userCtrl.update);
 
router.route('/:requestId').get(userCtrl.get);

router.route('/updateStatus/:requestId').put(userCtrl.updateStatus);

router.param('requestId', userCtrl.load);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });
  
export default router;
