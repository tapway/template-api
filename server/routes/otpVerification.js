import express from 'express';
import otpCtrl from '../controllers/otpVerification'

const router = express.Router();

router.route('/otpVerifyEndUser').post(otpCtrl.otpVerifyEndUser);

router.route('/otpVerifyDeliveryUser').post(otpCtrl.otpVerifyDeliveryUser);

router.route('/register').post(otpCtrl.register);

router.route('/resendOtp').post(otpCtrl.resendOtp);

export default router;