import express from "express";
import userCtrl from "../controllers/adminuser";
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';

const router = express.Router();

router.route("/create").post(userCtrl.create);

/**
 * Routes For Admin
 */

router.route("/").get(userCtrl.get);

router.route("/:userId").get(userCtrl.get);

router.route("/removeAll").get(userCtrl.removeAll);

router.param("userId", userCtrl.load);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });

router.route("/updateRating").put(userCtrl.updateRating);

router.route("/update/:userId").put(userCtrl.update);

router.route("/newAddress").put(userCtrl.addNewAddress);

export default router;
