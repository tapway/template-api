import express from 'express';
import serverConfigController from '../controllers/serverConfig';

const router = express.Router();

router.route('/').get(serverConfigController.get);

router.route('/create').post(serverConfigController.create);

router.route('/update').put(serverConfigController.update);

  
export default router;
