import express from "express";
import shopCtrl from "../controllers/shops";
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';

const router = express.Router();

router.route('/removeAll').get(shopCtrl.removeAll);

router.route('/deleteShop').put(shopCtrl.deleteShop);

router.route('/getAllShops').post(shopCtrl.getAllShops);

router.route('/getNearbyShopsByFilter').post(shopCtrl.getNearbyShopsByFilter);

router.route('/update').put(shopCtrl.update);

router.route('/create').post(shopCtrl.create);

router.route('/:shopId').get(shopCtrl.get);

router.param('shopId', shopCtrl.load);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });

export default router;