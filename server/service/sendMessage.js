import config from "../../config/env";
import serverConfig from "../models/serverConfig";
/**
 * This is a protected route. Will return random number.
 * @param phoneNumber
 * @returns {otp}
 */
function sendMessage(phoneNumber, otp) {
  serverConfig
    .findOne({ key: "twilio" })
    .execAsync()
    .then(twilioConfig => {
      if (twilioConfig) {
        const accountSid = twilioConfig.value.accountSid;
        const authToken = twilioConfig.value.authToken;
        const client = require("twilio")(accountSid, authToken);

        client.messages.create({
          body: "For registration enter the otp " + otp,
          from: "+12028758289",
          to: phoneNumber
        });
      }
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in fetching twilio config ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      throw err;
    });;;
  return otp;
}

export default sendMessage;
