import fetch from "node-fetch";
import endUserSchema from "../models/enduser";
import serverConfig from "../models/serverConfig";
import deliveryUserSchema from "../models/deliveryuser";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

const url = `https://onesignal.com/api/v1/notifications`;

function sendNotification(
  userId,
  notification,
  endUser,
  extraData = { source: "message" }
) {
  serverConfig
    .findOne({ key: "onesignal" })
    .execAsync()
    .then(onesignalConfig => {
      if (onesignalConfig) {
        const App_id = endUser
          ? onesignalConfig.value.onesignal_endUser_app_id
          : onesignalConfig.value.onesignal_deliveryUser_app_id;
        const Api_key = endUser
          ? onesignalConfig.value.onesignal_endUser_api_key
          : onesignalConfig.value.onesignal_deliveryUser_api_key;
        if (endUser) {
          endUserSchema.findOneAsync({ _id: userId }).then(userObj => {
            fetch(url, {
              method: "POST",
              body: JSON.stringify({
                app_id: App_id,
                contents: { en: notification },
                include_player_ids: [userObj.deviceInfo.userId], //userObj.deviceInfo.userId
                data: extraData
              }),
              headers: {
                "Content-Type": "application/json",
                Authorization: "Basic " + Api_key
              }
            })
              .then(res => res.json())

              .catch(errMessage => {
                const err = new APIError(
                  `error in sending notification ${errMessage}`,
                  httpStatus.INTERNAL_SERVER_ERROR
                );
                throw err;
              });
          });
        } else {
          deliveryUserSchema.findOneAsync({ _id: userId }).then(userObj => {
            fetch(url, {
              method: "POST",
              body: JSON.stringify({
                app_id: App_id,
                contents: { en: notification },
                include_player_ids: [userObj.deviceInfo.userId], //userObj.deviceInfo.userId
                data: { source: "message" }
              }),
              headers: {
                "Content-Type": "application/json",
                Authorization: "Basic " + Api_key
              }
            })
              .then(res => res.json())

              .catch(errMessage => {
                const err = new APIError(
                  `error in sending notification ${errMessage}`,
                  httpStatus.INTERNAL_SERVER_ERROR
                );
                throw err;
              });
          });
        }
      }
    })
    .catch(errMessage => {
      const err = new APIError(
        `error in fetching one signal config ${errMessage}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      throw err;
    });;
}
export default sendNotification;
