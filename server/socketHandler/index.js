import orderHandler from "./story/orderhandler";
import requestHandler from "./story/requesthandler";
import wathchGps from "./story/gpsLocationHandler";
import ratingHandler from "./story/ratingHandler";
import payment from "./story/payment";
import deliveryStatus from "./story/deliveryStatus";
import SocketStore from "./socketstore";
import trackDetails from "./story/fetchTrackingDetails";

const socketHandler = (socket, type) => {
  orderHandler(socket);
  requestHandler(socket);
  wathchGps(socket);
  trackDetails(socket);
  ratingHandler(socket);
  deliveryStatus(socket);
  payment(socket);

  socket.on("hello", () => {
    socket.emit("helloResponse", "hello everyone");
  });

  socket.on("disconnect", () => {
    if (type == "delivery") {
      SocketStore.removeByDeliveryId(socket.userId, socket);
    }
    if (type == "user") {
      SocketStore.removeByUserId(socket.userId, socket);
    }
  });
};

export default socketHandler;
