import mongoose from "mongoose";
import Orders from "../../models/orders";
import deliverySchema from "../../models/deliveryuser";
import SocketStore from "../socketstore";
import { listening } from "./rr";
import httpStatus from "http-status";
import APIError from "../../helpers/APIError";

export default function orderHandler(socket) {
  socket.on("makeOrder", function(data) {
    if (data.success) {
      var date = new Date();
      listening({ driver: { id: data.deliveryId }, response: true });
      const order = new Orders({
        _id: new mongoose.Types.ObjectId(),
        userId: data.userId._id,
        deliveryId: data.deliveryId,
        deliveryLocation: data.deliveryLocation,
        pickUpLocation: data.pickUpLocation,
        driverGpsLocation: data.pos,
        processingStatus: data.processingStatus._id,
        deliveryAddress: data.deliveryAddress,
        paymentType: data.paymentMode,
        otp: Math.floor(1000 + Math.random() * 9000),
        deliveryInstructions: data.deliveryInstructions,
        paymentAmount: data.paymentAmount,
        userRating: data.userRating,
        deliveryRating: data.deliveryRating,
        userReview: data.userReview,
        deliveryReview: data.deliveryReview,
        shopReview: data.shopReview,
        shopRating: data.shopRating,
        anyIssue: data.anyIssue,
        pickUpAddress: data.pickUpAddress,
        category: data.category._id,
        createdAt: date,
        updatedAt: date
      });
      order
        .saveAsync()
        .then(savedorder => {
          const returnObj = {
            success: true,
            message: "saved order",
            data: { savedorder }
          };
          socket.emit("getOrder", savedorder);
          deliverySchema
            .findById(savedorder.deliveryId)
            .execAsync()
            .then(user => {
              if (user) {
                Orders.get(savedorder._id)
                  .then(populatedOrder => {
                    return SocketStore.emitByUserId(
                      savedorder.userId,
                      "tripAccepted",
                      {
                        _id: savedorder._id,
                        fullName: user.fullName,
                        phoneNumber: user.phoneNumber,
                        profileImage: user.profileImage,
                        rating: user.rating,
                        carNumber: user.vehicleDetails.vehicleNumber,
                        otp: savedorder.otp,
                        driverGpsLocation: user.gpsLocation,
                        pickUpLocation: savedorder.pickUpLocation,
                        deliveryLocation: savedorder.deliveryLocation,
                        paymentAmount: savedorder.paymentAmount,
                        category: populatedOrder.category.name,
                        processingStatus: populatedOrder.processingStatus.status
                      }
                    );
                  })
                  .error(errMessage => {
                    const err = new APIError(
                      `error in finding populated order ${errMessage}`,
                      httpStatus.INTERNAL_SERVER_ERROR
                    );
                    throw err;
                  });
              }
            })
            .error(errMessage => {
              const err = new APIError(
                `error in finding delivery user ${errMessage}`,
                httpStatus.INTERNAL_SERVER_ERROR
              );
              throw err;
            });
        })
        .error(errMessage => {
          const err = new APIError(
            `error in saving order ${errMessage}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          throw err;
        });
    } else {
      listening({ driver: { id: data.deliveryId }, response: false });
    }
  });
}
