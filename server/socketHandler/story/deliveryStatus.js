import OrderSchema from "../../models/orders";
import OrderStatusSchema from "../../models/orderstatus";
import SocketStore from "../socketstore";
import deliverySchema from "../../models/deliveryuser";
import sendNotification from "../../service/pushNotification";
import httpStatus from "http-status";
import APIError from "../../helpers/APIError";

export default function deliveryStatus(socket) {
  socket.on("deliveryStatus", function(data) {
    var date = new Date();
    OrderSchema.findById(data.orderId)
      .execAsync()
      .then(order => {
        if (data.status === "Completed") {
          order.deliveredDateTime = date;
        }
        order
          .saveAsync()
          .then(savedOrder => {
            deliverySchema
              .findById(savedOrder.deliveryId)
              .execAsync()
              .then(user => {
                OrderStatusSchema.findById(data.statusId)
                  .execAsync()
                  .then(status => {
                    status.status = data.status;
                    status
                      .saveAsync()
                      .then(status => {
                        sendNotification(
                          data.userId,
                          "Your Order Is " + status.status,
                          true,
                          {
                            status: status.status,
                            trip: {
                              _id: savedOrder._id,
                              fullName: user.fullName,
                              phoneNumber: user.phoneNumber,
                              profileImage: user.profileImage,
                              rating: user.rating,
                              carNumber: user.vehicleDetails.vehicleNumber,
                              otp: savedOrder.otp,
                              driverGpsLocation: user.gpsLocation,
                              pickUpLocation: savedOrder.pickUpLocation,
                              deliveryLocation: savedOrder.deliveryLocation,
                              paymentAmount: savedOrder.paymentAmount
                            }
                          }
                        );
                        SocketStore.emitByUserId(
                          data.userId,
                          "UpdateOrderStatus",
                          status.status
                        );
                      })
                      .error(errMessage => {
                        const err = new APIError(
                          `error in saving order status ${errMessage}`,
                          httpStatus.INTERNAL_SERVER_ERROR
                        );
                        throw err;
                      });
                  })
                  .error(errMessage => {
                    const err = new APIError(
                      `error in finding order status ${errMessage}`,
                      httpStatus.INTERNAL_SERVER_ERROR
                    );
                    throw err;
                  });
              });
          })
          .error(errMessage => {
            const err = new APIError(
              `error in finding delivery user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            throw err;
          });
      })
      .error(errMessage => {
        const err = new APIError(
          `error in finding order ${errMessage}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        throw err;
      });
  });
}
