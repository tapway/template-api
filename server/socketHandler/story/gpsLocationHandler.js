import Deliveryuser from "../../models/deliveryuser";
import Orders from "../../models/orders";
import SocketStore from "../socketstore";
import httpStatus from "http-status";
import APIError from "../../helpers/APIError";

export default function wathchGps(socket) {
  socket.on("updateLocation", function(data) {
    Deliveryuser.findById(data.id)
      .execAsync()
      .then(result => {
        result.gpsLocation = data.coords;
        result
          .saveAsync()
          .then(savedUser => {
            if (savedUser.onTrip) {
              Orders.getActiveByDeliveryId(savedUser._id).then(order => {
                order.driverGpsLocation = savedUser.gpsLocation;
                order
                  .saveAsync()
                  .then(savedOrder => {
                    return SocketStore.emitByUserId(
                      savedOrder.userId._id,
                      "TripUpdates",
                      {
                        _id: savedOrder._id,
                        fullName: savedUser.fullName,
                        phoneNumber: savedUser.phoneNumber,
                        profileImage: savedUser.profileImage,
                        rating: savedUser.rating,
                        carNumber: savedUser.vehicleDetails.vehicleNumber,
                        otp: savedOrder.otp,
                        driverGpsLocation: savedUser.gpsLocation,
                        pickUpLocation: savedOrder.pickUpLocation,
                        deliveryLocation: savedOrder.deliveryLocation,
                        paymentAmount: savedOrder.paymentAmount,
                        category: order.category.name,
                        processingStatus: order.processingStatus.status
                      }
                    );
                  })
                  .error(errMessage => {
                    const err = new APIError(
                      `error in saving order after location update ${errMessage}`,
                      httpStatus.INTERNAL_SERVER_ERROR
                    );
                    throw err;
                  });
              });
            }
          })
          .error(errMessage => {
            const err = new APIError(
              `error in saving delivery user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            throw err;
          });
      })
      .error(errMessage => {
        const err = new APIError(
          `error in finding delivery user ${errMessage}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        throw err;
      });
  });
}
