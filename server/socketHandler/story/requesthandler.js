import mongoose from "mongoose";
import Requests from "../../models/requests";
import Categories from "../../models/categories";
import Orderstatus from "../../models/orderstatus";
import { getDistance } from "geolib";
import { requestTripHandler } from "./rr";
import deliveryUserSchema from "../../models/deliveryuser";
import httpStatus from "http-status";
import APIError from "../../helpers/APIError";
import config from "../../../config/env";
import _ from "lodash";

export default function requestHandler(socket) {
  const deliveryreq = (user, category) => {
    deliveryUserSchema
      .findAsync({
        $and: [
          {
            gpsLocation: {
              $geoWithin: {
                $centerSphere: [user.pickUpLocation, config.radius]
              }
            }
          },
          {
            isAvailable: true
          },
          {
            onTrip: false
          }
        ]
      })
      .then(result => {
        var arr = _.orderBy(
          result,
          function(o) {
            return getDistance(
              {
                latitude: user.pickUpLocation[1],
                longitude: user.pickUpLocation[0]
              },
              { latitude: o.gpsLocation[1], longitude: o.gpsLocation[0] }
            );
          },
          ["desc"]
        );
        requestTripHandler(socket, arr, user, category);
      })
      .error(errMessage => {
        const err = new APIError(
          `error in finding delivery user within 20 km ${errMessage}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        throw err;
      });
  };

  socket.on("requestTrip", function(data) {
    const orderstatus = new Orderstatus({
      _id: new mongoose.Types.ObjectId(),
      status: "Pending"
    });
    const category = new Categories({
      _id: new mongoose.Types.ObjectId(),
      name: data.category,
      img: data.categoryimg
    });
    orderstatus
      .saveAsync()
      .then(savedstatus => {
        category
          .saveAsync()
          .then(savedcategory => {
            const request = new Requests({
              userId: data.userId,
              category: category._id,
              processingStatus: orderstatus._id,
              paymentMode: data.paymentMode,
              deliveryInstructions: data.deliveryInstructions,
              paymentAmount: data.paymentAmount,
              endUserGpsLocation: data.endUserGpsLocation,
              deliveryLocation: data.deliveryLocation,
              pickUpLocation: data.pickUpLocation,
              deliveryAddress: data.deliveryAddress,
              pickUpAddress: data.pickUpAddress
            });
            request
              .saveAsync()
              .then(savedreq => {
                Requests.get(savedreq._id)
                  .then(user => {
                    deliveryreq(user, data.category);
                  })
                  .error(errMessage => {
                    const err = new APIError(
                      `error in geting request ${errMessage}`,
                      httpStatus.INTERNAL_SERVER_ERROR
                    );
                    throw err;
                  });
              })
              .error(errMessage => {
                const err = new APIError(
                  `error in saving request ${errMessage}`,
                  httpStatus.INTERNAL_SERVER_ERROR
                );
                throw err;
              });
          })
          .error(errMessage => {
            const err = new APIError(
              `error in saving category ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            throw err;
          });
      })
      .error(errMessage => {
        const err = new APIError(
          `error in saving order status ${errMessage}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        throw err;
      });
  });
}
