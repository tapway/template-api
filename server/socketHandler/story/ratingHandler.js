import DeliverySchema from "../../models/deliveryuser";
import UserSchema from "../../models/enduser.js";
import Orders from "../../models/orders.js";
import httpStatus from "http-status";
import APIError from "../../helpers/APIError";

export default function ratingHandler(socket) {
  socket.on("DeliveryUserRating", data => {
    Orders.findById(data._id)
      .execAsync()
      .then(order => {
        var date = new Date();
        order.deliveryRating = data.deliveryRating;
        order.deliveryReview = data.deliveryReview;
        order.shopReview = data.shopReview;
        order.shopRating = data.shopRating;
        order.deliveredDateTime = date;
        order
          .saveAsync()
          .then(savedOrder => {
            // calculate delivery guy's average rating
            Orders.aggregate([
              {
                $match: {
                  deliveryId: savedOrder.deliveryId,
                  deliveryRating: {
                    $gt: 0
                  }
                }
              },
              {
                $group: {
                  _id: "$deliveryId",
                  rating: {
                    $avg: "$deliveryRating"
                  }
                }
              }
            ])
              .then(result => {
                if (result) {
                  DeliverySchema.findById(result[0]._id)
                    .execAsync()
                    .then(deliveryGuy => {
                      deliveryGuy.rating = result[0].rating;
                      deliveryGuy.saveAsync().error(errMessage => {
                        const err = new APIError(
                          `error in saving delivery user ${errMessage}`,
                          httpStatus.INTERNAL_SERVER_ERROR
                        );
                        throw err;
                      });
                    })
                    .error(errMessage => {
                      const err = new APIError(
                        `error in finding delivery user ${errMessage}`,
                        httpStatus.INTERNAL_SERVER_ERROR
                      );
                      throw err;
                    });
                }
              })
          })
          .error(errMessage => {
            const err = new APIError(
              `error in saving order ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            throw err;
          });
      })
      .error(errMessage => {
        const err = new APIError(
          `error in finding order ${errMessage}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        throw err;
      });
  });

  socket.on("EndUserRating", data => {
    Orders.findById(data._id)
      .execAsync()
      .then(order => {
        order.userRating = data.userRating;
        order.userReview = data.userReview;
        order
          .saveAsync()
          .then(savedOrder => {
            Orders.aggregate([
              {
                $match: {
                  userId: savedOrder.userId,
                  userRating: { $gt: 0 }
                }
              },
              {
                $group: {
                  _id: "$userId",
                  rating: { $avg: "$userRating" }
                }
              }
            ])
              .then(result => {
                if (result) {
                  UserSchema.findById(result[0]._id)
                    .execAsync()
                    .then(user => {
                      user.rating = result[0].rating;
                      user.saveAsync().error(errMessage => {
                        const err = new APIError(
                          `error in saving user ${errMessage}`,
                          httpStatus.INTERNAL_SERVER_ERROR
                        );
                        throw err;
                      });
                    })
                    .error(errMessage => {
                      const err = new APIError(
                        `error in finding user ${errMessage}`,
                        httpStatus.INTERNAL_SERVER_ERROR
                      );
                      throw err;
                    });
                }
              })
          })
          .error(errMessage => {
            const err = new APIError(
              `error in saving order ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            throw err;
          });
      })
      .error(errMessage => {
        const err = new APIError(
          `error in finding order ${errMessage}`,
          httpStatus.INTERNAL_SERVER_ERROR
        );
        throw err;
      });
  });
}
