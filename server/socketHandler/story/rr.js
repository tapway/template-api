import SocketStore from "../socketstore";
import deferred from "deferred";
import Promise from "bluebird";
import sendNotification from "../../service/pushNotification";

const watchId = {};
const promObj = {};

function listening(data) {
  clearInterval(watchId[data.driver.id]);
  promObj[data.driver.id].resolve(data.response);
}

function requestTripHandler(socket, nearByDrivers, requestObj, category) {
  const quantum = 20;
  for (let i = nearByDrivers.length - 1; i >= 0; i--) {
    if (!checkSocketConnection(nearByDrivers[i].id)) {
      nearByDrivers = removeDriverFromList(nearByDrivers, i);
    }
  }
  roundRobinAsync(nearByDrivers, quantum, requestObj, category)
    .then(result => {
      if (!result) {
        socket.emit('noNearbyDriver');
      }
    })
    .catch(e => console.log("error in rr", e));
}

function roundRobinAsync(nearByDrivers, quantum, requestObj, category) {
  return new Promise((resolve, reject) => {
    const count = 0;
    const remain = nearByDrivers.length;
    const prom = deferred();
    dispatchHandlerAsync(
      nearByDrivers,
      quantum,
      remain,
      count,
      requestObj,
      prom,
      category
    )
      .then(result => resolve(result))
      .catch(error => reject(error));
  });
}

function dispatchHandlerAsync(
  nearByDrivers,
  quantum,
  remain,
  count,
  requestObj,
  prom,
  category
) {

  if (remain <= 0) {
    prom.resolve(false);
    return prom.promise;
  }
  promObj[nearByDrivers[count].id] = deferred();
  sendRequestAsync(
    nearByDrivers[count],
    quantum,
    requestObj,
    promObj[nearByDrivers[count].id],
    category
  ).then(
    response => {
      if (response) {
        prom.resolve(response);
      } else {
        nearByDrivers = removeDriverFromList(nearByDrivers, count);
        count = 0;
        remain--;
        setTimeout(() => {
          dispatchHandlerAsync(
            nearByDrivers,
            quantum,
            remain,
            count,
            requestObj,
            prom
          );
        }, 1000);
      }
    },
    () => {
      nearByDrivers = removeDriverFromList(nearByDrivers, count);
      count = 0;
      remain--;
      setTimeout(() => {
        dispatchHandlerAsync(
          nearByDrivers,
          quantum,
          remain,
          count,
          requestObj,
          prom
        );
      }, 1000);
    }
  );
  return prom.promise;
}

function sendRequestAsync(driver, quantum, requestObj, def, category) {
  SocketStore.emitByDeliveryId(driver.id, "Trip", requestObj);
  sendNotification(driver.id, category + " Request", false, { newOrder: true });
  watchId[driver.id] = setInterval(() => {
    quantum--;
    if (quantum <= 0) {
      clearInterval(watchId[driver.id]);
      SocketStore.emitByDeliveryId(driver.id, "responseTimedOut");
      def.reject("no response from driver");
    }
  }, 1000);
  return def.promise;
}

function checkSocketConnection(id) {
  const res = SocketStore.getByDeliveryId(id);
  if (res.success && res.data.length) {
    return true;
  } else {
    return false;
  }
}

function removeDriverFromList(drivers, index) {
  // test passed
  return drivers.slice(0, index).concat(drivers.slice(index + 1));
}

export default { requestTripHandler, listening };
