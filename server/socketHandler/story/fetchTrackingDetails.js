import Deliveryuser from "../../models/deliveryuser";
import Orders from "../../models/orders";
import SocketStore from "../socketstore";
import httpStatus from "http-status";
import APIError from "../../helpers/APIError";

export default function trackDetails(socket) {
  socket.on("fetchTrackDetails", function(data) {
    Orders.get(data.id)
      .then(order => {
        Deliveryuser.findById(order.deliveryId._id)
          .execAsync()
          .then(user => {
            SocketStore.emitByUserId(order.userId._id, "trackDetailsResult", {
              _id: order._id,
              fullName: user.fullName,
              phoneNumber: user.phoneNumber,
              profileImage: user.profileImage,
              rating: user.rating,
              carNumber: user.vehicleDetails.vehicleNumber,
              otp: order.otp,
              driverGpsLocation: user.gpsLocation,
              pickUpLocation: order.pickUpLocation,
              deliveryLocation: order.deliveryLocation,
              paymentAmount: order.paymentAmount,
              category: order.category.name,
              processingStatus: order.processingStatus.status
            });
          })
          .error(errMessage => {
            const err = new APIError(
              `error in finding delivery user ${errMessage}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            throw err;
          });
      });
  });
}
