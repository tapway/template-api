import mongoose from "mongoose";
import { find } from "lodash";

const OrderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.ObjectId,
    ref: "Enduser",
    default: null
  },
  deliveryId: {
    type: mongoose.Schema.ObjectId,
    ref: "DeliveryUser",
    default: null
  },
  shopId: {
    type: mongoose.Schema.ObjectId,
    ref: "Shops",
    default: null
  },
  deliveryInstructions: {
    type: Object,
    default: null
  },
  otp: {
    type: Number
  },
  deliveryLocation: [],
  pickUpLocation: [],
  driverGpsLocation: [],
  processingStatus: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Orderstatus",
    default: null
  },
  deliveryAddress: {
    type: String,
    default: null
  },
  paymentType: {
    type: String,
    default: null
  },
  paymentAmount: {
    type: Number,
    default: null
  },
  userRating: {
    type: Number,
    default: 0
  },
  deliveryRating: {
    type: Number,
    default: 0
  },
  userReview: {
    type: String,
    default: null
  },
  deliveryReview: {
    type: String,
    default: null
  },
  shopReview: {
    type: String,
    default: null
  },
  shopRating: {
    type: Number,
    default: 0
  },
  anyIssue: {
    type: Boolean,
    default: null
  },
  pickUpAddress: {
    type: String,
    default: null
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Categories",
    default: null
  },
  deliveredDateTime: {
    type: Date,
    default: null
  },
  isCanceled: {
    type: Boolean,
    default: false
  },
  cancelReason: {
    type: String,
    default: null
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  }
});

// /**
//  * Statics
//  */
OrderSchema.statics = {
  /**
   * Get status
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate("userId")
      .populate("category")
      .populate("processingStatus")
      .execAsync()
      .then(user => {
        if (user) {
          return user;
        }
      });
  },
  list(skip = 0, limit = 50, id, type) {
    if (type === "delivery") {
      return this.find({ deliveryId: id })
        .populate("category")
        .sort({ createdAt: -1 })
        .skip(parseInt(skip))
        .limit(parseInt(limit))
        .execAsync()
        .then(user => {
          if (user) {
            return user;
          }
        });
    } else {
      return this.find({ userId: id })
        .populate("userId")
        .populate("category")
        .populate("processingStatus")
        .sort({ createdAt: -1 })
        .skip(parseInt(skip))
        .limit(parseInt(limit))
        .execAsync()
        .then(user => {
          if (user) {
            return user;
          }
        });
    }
  },
  getActiveByDeliveryId(id) {
    return this.find({ deliveryId: id })
      .populate("userId")
      .populate("category")
      .populate("processingStatus")
      .execAsync()
      .then(users => {
        const result = find(users, user => {
          return (
            user.processingStatus.status === "Accepted" ||
            user.processingStatus.status === "Started" ||
            user.processingStatus.status === "Picked"
          );
        });
        return result;
      });
  }
};

/**
 * @typedef DeliveryUser
 */
export default mongoose.model("Orders", OrderSchema);
