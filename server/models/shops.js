import mongoose from "mongoose";
import _ from "lodash";

const ShopsSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    default: null
  },
  address: {
    type: String,
    default: null
  },
  location: [],
  openingTime: {
    type: Object,
    default: null
  },
  closingTime: {
    type: Object,
    default: null
  },
  contact: {
    type: String,
    default: null
  },
  rating:{
    type:Number,
    default:0
  },
  shopType: {
    type: String,
    enum: ["Food", "Pets", "Groceries", "Medical", "Electronics", "Sports"],
    default: null
  }
});

// /**
//  * Statics
//  */
ShopsSchema.statics = {
  /**
   * Get shop
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(shop => {
        if (shop) {
          return shop;
        }
      });
  },
  list(skip = 0, limit = 50, location, shopType) {
    return this.findAsync({
      shopType,
      $and: [
        {
          location: {
            $geoWithin: {
              $centerSphere: [
                [parseFloat(location[0]), parseFloat(location[1])],
                10 / 3963.2
              ]
            }
          }
        }
      ]
    }).then(result => {
      var arr = _.sortBy(result, [
        function(o) {
          return o;
        }
      ]);
      return arr.splice(skip, limit);
    });
  },
  listNearbyShopsWithFilter(skip = 0, limit = 50, location, shopType, query) {
    return this.findAsync({
      shopType,
      $and: [
        {
          location: {
            $geoWithin: {
              $centerSphere: [
                [parseFloat(location[0]), parseFloat(location[1])],
                10 / 3963.2
              ]
            }
          }
        }
      ]
    }).then(result => {
      var arr = _.sortBy(result, [
        function(o) {
          return o;
        }
      ]);
      const filteredArray = arr.filter(item => {
        return item.name.toLowerCase().includes(query.toLowerCase());
      });
      return filteredArray.splice(skip, limit);
    });
  },
  listAllShops(skip = 0, limit = 50) {
    return this.findAsync()
      .sort({ createdAt: -1 })
      .skip(parseInt(skip))
      .limit(parseInt(limit))
      .execAsync()
      .then(items => {
        if (items) {
          return items;
        }
      });
  }
};

export default mongoose.model("Shops", ShopsSchema);
