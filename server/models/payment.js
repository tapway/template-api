import mongoose from "mongoose";

const paymentSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  paymentId: {
    type: String,
    default: null
  },
  amount: {
    type: Number,
    default: null
  },
  orderId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Orders",
    default: null
  },
  verified: {
      type:Boolean,
      default:false
  },
  invoice_id:{
    type: String,
    default: null
  },
  type:{
    type: String,
    default: null
  },
});

// /**
//  * Statics
//  */
paymentSchema.statics = {
  /**
   * Get item
   * @param {ObjectId} id - The objectId of user.
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(payment => {
        if (payment) {
          return payment;
        }
      });
  },
};

export default mongoose.model("Payment", paymentSchema);
