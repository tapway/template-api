import Promise from "bluebird";
import mongoose from "mongoose";

/**
 * Delivery Schema
 */
const DeliveryUserSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  fullName: {
    type: String
  },
  dob: {
    type: String
  },
  gender: {
    type: String
  },
  currency: {
    type: String
  },
  isLoggedIn: {
    type: Boolean
  },
  language: {
    type: String
  },
  address: {
    type: String
  },
  gpsLocation: {
    type: Array,
    default: []
  },
  savedAddresses: {
    type: Array,
    default: []
  },
  phoneNumber: {
    type: Number
  },
  email: {
    type: String
  },
  profileImage: {
    type: String
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  verificationDetails: {
    type: Object,
    default: null
  },
  verifictaionImage: {
    type: String
  },
  vehicleVerificationImage: {
    type: String
  },
  vehicleDetails: {
    type: Object
  },
  isAvailable: {
    type: Boolean,
    default: true
  },
  onTrip: {
    type: Boolean,
    default: false
  },
  bankDetails: {
    type: Object,
    default: null
  },
  otp: {
    type: Number
  },
  rating: {
    type: Number,
    default: 0
  },
  jwtAccessToken: {
    type: String,
    default: null
  },
  deviceInfo: {
    type: Object,
    default: null
  },
});

/**
 * Statics
 */
DeliveryUserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(user => {
        if (user) {
          return user;
        }
      });
  },
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit)
      .execAsync();
  }
};

/**
 * @typedef DeliveryUser
 */
export default mongoose.model("Deliveryuser", DeliveryUserSchema);
