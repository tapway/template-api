import mongoose from 'mongoose';

/**
 * User Schema
 */
const otpVerificationSchema = new mongoose.Schema({
  phoneNumber:{
      type:String,
  },
  otp:{
      type:Number,
  }
});

/**
 * Statics
 */
otpVerificationSchema.statics = {
  /**
   * Get status
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(icomingObject) {
    return this.findOneAsync(icomingObject)
      .execAsync().then((res) => {
        if (res) {
          return res;
        }
      });
  },
};


/**
 * @typedef User
 */
export default mongoose.model('OtpVerification', otpVerificationSchema);
