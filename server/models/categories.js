import mongoose from 'mongoose';

/**
 * User Schema
 */
const CategoriesSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name:{
      type:String,
  },
  img:{
      type:String,
  }
});

/**
 * Statics
 */
CategoriesSchema.statics = {
  /**
   * Get status
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync().then((user) => {
        if (user) {
          return user;
        }
      });
  },
};


/**
 * @typedef User
 */
export default mongoose.model('Categories', CategoriesSchema);
