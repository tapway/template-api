import mongoose from "mongoose";
import { indexOf, orderBy } from "lodash";

const itemsSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    default: null
  },
  price: {
    type: Number,
    default: null
  },
  pic: {
    type: String,
    default: null
  },
  itemType: {
    type: String,
    enum: ["Veg", "NonVeg", "Product"],
    default: "Product"
  },
  brand: {
    type: String,
    default: null
  },
  shopId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Shops",
    default: null
  }
});

// /**
//  * Statics
//  */
itemsSchema.statics = {
  /**
   * Get item
   * @param {ObjectId} id - The objectId of user.
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(item => {
        if (item) {
          return item;
        }
      });
  },
  list(skip = 0, limit = 50, shopId, itemType) {
    return this.find({ shopId, itemType })
      .sort({ createdAt: -1 })
      .skip(parseInt(skip))
      .limit(parseInt(limit))
      .execAsync()
      .then(items => {
        if (items) {
          return items;
        }
      });
  },
  listByQuery(skip = 0, limit = 50, shopId, itemType, query) {
    return this.find({ shopId, itemType })
      .execAsync()
      .then(data => {
        const result = data.filter(item => {
          return item.name.toLowerCase().includes(query.toLowerCase());
        });
        return result.splice(skip, limit);
      });
  },
  listByFilters(skip = 0, limit = 50, shopId, itemType, query, filters) {
    return this.find({ shopId, itemType })
      .execAsync()
      .then(data => {
        let result = data.filter(item => {
          if (filters.brands.length===0) {
            return (
              item.price >= filters.range[0] &&
              item.price <= filters.range[1] &&
              item.name.toLowerCase().includes(query.toLowerCase())
            );
          } else {
            return (
              indexOf(filters.brands, item.brand) !== -1 &&
              (item.price >= filters.range[0] &&
                item.price <= filters.range[1]) &&
              item.name.toLowerCase().includes(query.toLowerCase())
            );
          }
        });
        if (filters.sort === 1) {
          result = orderBy(result, ["price"], ["asc"]);
        }
        if (filters.sort === 2) {
          result = orderBy(result, ["price"], ["desc"]);
        }
        return result.splice(skip, limit);
      });
  }
};

export default mongoose.model("Items", itemsSchema);
