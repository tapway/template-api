import mongoose from 'mongoose';

/**
 * User Schema
 */
const OrderstatusSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  status:{
      type:String,
      enum:['Pending','Processing','Accepted','Rejected','Started','Picked','Completed','Failed'],
      default:'Pending'
  }
});


/**
 * Statics
 */
OrderstatusSchema.statics = {
  /**
   * Get status
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync().then((user) => {
        if (user) {
          return user;
        }
      });
  },
};


/**
 * @typedef User
 */
export default mongoose.model('Orderstatus', OrderstatusSchema);
