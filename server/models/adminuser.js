import Promise from "bluebird";
import mongoose from "mongoose";
const bcrypt = require('bcrypt');
//SALT_WORK_FACTOR = 10;
/**
 * User Schema
 */
const AdminUserSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  fullName: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  address: {
    type: String,
    default: null
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  gpsLocation: {
    type: Array,
    default: []
  },
  savedAddresses: {
    type: Array,
    default: []
  },
  phoneNumber: {
    type: String
  },
  otp: {
    type: Number
  },
  email: {
    type: String
  },
  password: {
    type: String,
    required: true,
  },
  active: {
    type: Boolean,
    required: false,
    default: false
  },
  profileImage: {
    type: String
  },
  rating: {
    type: Number,
    default: 0
  },
  jwtAccessToken: {
    type: String,
    default: null
  },
  deviceInfo: {
    type: Object,
    default: null
  },
});


AdminUserSchema
    .virtual('pass')
    // set methods
    .set(function (password) {
      this._password = password;
    });

/**
 * Statics
 */
AdminUserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(user => {
        if (user) {
          return user;
        }
      });
  }
};

AdminUserSchema.pre('save', function (next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(10, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

AdminUserSchema.methods.validatePassword = function validatePassword(data) {
  console.log(this.username)
  return bcrypt.compareSync(data, this.password);
};

AdminUserSchema.methods.comparePassword = function (candidatePassword, cb) {
  var user = this;
  bcrypt.compare(candidatePassword, user.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

AdminUserSchema.methods.isPasswordValid = function (password) {
  return bcrypt.compareSync(password, this.password);
};

/**
 * @typedef User
 */
export default mongoose.model("AdminUser", AdminUserSchema);
