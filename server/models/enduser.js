import Promise from "bluebird";
import mongoose from "mongoose";
/**
 * User Schema
 */
const EnduserSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  fullName: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  address: {
    type: String,
    default: null
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  gpsLocation: {
    type: Array,
    default: []
  },
  savedAddresses: {
    type: Array,
    default: []
  },
  phoneNumber: {
    type: String
  },
  otp: {
    type: Number
  },
  email: {
    type: String
  },
  profileImage: {
    type: String
  },
  rating: {
    type: Number,
    default: 0
  },
  jwtAccessToken: {
    type: String,
    default: null
  },
  deviceInfo: {
    type: Object,
    default: null
  },
});
/**
 * Statics
 */
EnduserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(user => {
        if (user) {
          return user;
        }
      });
  }
};
/**
 * @typedef User
 */
export default mongoose.model("Enduser", EnduserSchema);
