import mongoose from "mongoose";
const Schema = mongoose.Schema;
import _ from "lodash"; //eslint-disable-line

/**
 * Server Config Schema
 */
const ServerConfigSchema = new mongoose.Schema({
  type: { type: Schema.Types.Mixed },
  key: { type: String, required: true, unique: true },
  value: { type: Schema.Types.Mixed }
});

// /**
//  * Statics
//  */
ServerConfigSchema.statics = {
  /**
   * Get server config
   * @param {ObjectId} id - The objectId of config.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then(config => {
        if (config) {
          return config;
        }
      });
  },
  getByKey(key) {
    return this.findOne({ key })
      .execAsync()
      .then(config => {
        if (config) {
          return config;
        }
      });
  }
};
/**
 * @typedef Config
 */
export default mongoose.model("ServerConfig", ServerConfigSchema);
