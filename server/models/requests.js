import mongoose from "mongoose";
/**
 * Request Schema
 */
const RequestSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Enduser",
    default: null
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Categories",
    default: null
  },
  processingStatus: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Orderstatus",
    default: null
  },
  deliveryInstructions:{
    type:Object,
    default:null,
  },
  paymentMode: {
    type: String,
    default: null
  },
  paymentAmount: {
    type: Number,
    default: null
  },
  endUserGpsLocation: {
    type: Array,
    default: []
  },
  packageSize: {
    type: String,
    default: null
  },
  itemQuantity: {
    type: String,
    default: null
  },
  deliveryAddress: {
    type: String,
    default: null
  },
  pickUpAddress: {
    type: String,
    default: null
  },
  deliveryLocation: {
    type: Array,
    default: []
  },
  pickUpLocation: {
    type: Array,
    default: []
  }
});
// /**
//  * Statics
//  */
RequestSchema.statics = {
  /**
   * Get status
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate("userId")
      .populate("category")
      .populate("processingStatus")
      .execAsync()
      .then(user => {
        if (user) {
          return user;
        }
      });
  }
};
/**
 * @typedef User
 */
export default mongoose.model("Requests", RequestSchema);
