const childProcess = require("child_process");

function runScript(scriptPath, callback) {
  // keep track of whether callback has been invoked to prevent multiple invocations
  let invoked = false;
  const process = childProcess.fork(scriptPath);
  // listen for errors as they may prevent the exit event from firing
  process.on("error", err => {
    if (invoked) return;
    invoked = true;
    callback(err);
  });
  // execute the callback once the process has finished running
  process.on("exit", code => {
    if (invoked) return;
    invoked = true;
    const err = code === 0 ? null : new Error("exit code", code);
    callback(err);
  });
}

runScript("./scripts/appConfig.js", err => {
  if (err) throw err;
  console.log("finished running appConfig.js");
});
runScript("./scripts/serverConfig.js", err => {
  if (err) throw err;
  console.log("finished running serverConfig.js");
});

runScript("./scripts/seedShops.js", err => {
  if (err) throw err;
  console.log("finished running seedShops.js");
});
