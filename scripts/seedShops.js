const async = require("async");
const faker = require("faker");
const readline = require("readline");
const Promise = require("bluebird");
var node_geocoder = require("node-geocoder");
const options = {
  provider: "google",
  // Optional depending on the providers
  httpAdapter: "https", // Default
  apiKey: "YOUR_GOOGLE_MAPS_API_KEY", // for Mapquest, OpenCage, Google Premier
  formatter: null // 'gpx', 'string', ...
};
var geocoder = node_geocoder(options);

// Import mongoose.js to define our schema and interact with MongoDB
const mongoose = require("mongoose");
Promise.promisifyAll(mongoose);

// const Schema = mongoose.Schema;
// Import bcrypt-nodejs for hashing passwords on MongoDB

// let databaseName = "taxiApp-development";
// const nodeEnv = process.env.NODE_ENV;
// if (nodeEnv === "development") {
//   databaseName = "taxiApp-development";
// }
// if (nodeEnv === "production") {
//   databaseName = "taxiApp-api-production";
// }

// URL to connect to a local MongoDB with database test.
// Change this to fit your running MongoDB instance
const databaseURL = `mongodb://localhost/delivery`;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let requiredLocation = [];
const item_count = 50,
  shop_count = 30;

const ShopSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    default: null
  },
  address: {
    type: String,
    default: null
  },
  location: [],
  openingTime: {
    type: Object,
    default: null
  },
  closingTime: {
    type: Object,
    default: null
  },
  contact: {
    type: String,
    default: null
  },
  rating: {
    type: Number,
    default: 0
  },
  shopType: {
    type: String,
    enum: ["Food", "Pets", "Groceries", "Medical", "Electronics", "Sports"],
    default: null
  }
});
const ItemSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    default: null
  },
  price: {
    type: Number,
    default: null
  },
  pic: {
    type: String,
    default: null
  },
  itemType: {
    type: String,
    enum: ["Veg", "NonVeg", "Product"],
    default: "Product"
  },
  brand: {
    type: String,
    default: null
  },
  shopId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Shops",
    default: null
  }
});
const Shop = mongoose.model("Shops", ShopSchema);
const Item = mongoose.model("Item", ItemSchema);

// Async series method to make sure asynchronous calls below run sequentially
async.series(
  [
    function(callback) {
      // Open connection to MongoDB
      mongoose.connect(databaseURL);
      // Need to listen to 'connected' event then execute callback method
      // to call the next set of code in the async.serial array
      mongoose.connection.on("connected", () => {
        console.log("db connected via mongoose");
        // Execute callback now we have a successful connection to the DB
        // and move on to the third function below in async.series
        callback(null, "SUCCESS - Connected to mongodb");
      });
    },
    function(callback) {
      rl.question(
        "Tell us about your location to set up the database accordingly...\nWarning: this will act as a center \n",
        answer => {
          // TODO: Log the answer in a database
          console.log(`Thank you for your valuable feedback: ${answer}`);

          geocoder
            .geocode(answer, function(err, data) {
              requiredLocation.push(data[0].longitude);
              requiredLocation.push(data[0].latitude);
            })
            .then(() => {
              callback(null, "Location fetched successfully");
            });
          rl.close();
        }
      );
    },
    function(callback) {
      const catagories = [
        "Food",
        "Groceries",
        "Medical",
        "Sports",
        "Pets",
        "Electronics"
      ];
      const Brands = [
        [],
        ["Parle", "Britania", "Cadbury", "Nestle", "Amul", "Nandini"],
        ["Synthroid", "Crestor", "Ventolin", "Nexium", "Lyrica", "Vyvanse"],
        ["Nike", "Adidas", "UndeArmour", "Puma", "Sketchers", "Asics"],
        [
          "Mars Petcare Inc",
          "Nestle",
          "Hill's Pet Nutrition",
          "UPG",
          "Unicharm Corp",
          "J M Smucker"
        ],
        ["HP", "Dell", "Bosch", "Apple", "One Plus", "Lenovo"]
      ];
      for (
        let catagory_iterator = 0;
        catagory_iterator < catagories.length;
        catagory_iterator++
      ) {
        for (
          let shops_iterator = 0;
          shops_iterator < shop_count;
          shops_iterator++
        ) {
          let res_location = [];
          const max = 0.006,
            min = 0.002;
          res_location.push(
            requiredLocation[0] + (Math.random() * (+max - +min) + +min)
          );
          res_location.push(
            requiredLocation[1] + (Math.random() * (+max - +min) + +min)
          );
          const shop = new Shop({
            _id: new mongoose.Types.ObjectId(),
            name: faker.company.companyName(),
            address: faker.address.streetAddress() + faker.address.streetName(),
            location: res_location,
            rating: Math.floor((Math.random() * (5 - 0) + 0) * 10) / 10,
            contact: faker.phone.phoneNumber(),
            openingTime: {
              hr: Math.floor(Math.random() * (+10 - +6) + +6),
              min: Math.floor(Math.random() * (+60 - +0) + +0)
            },
            closingTime: {
              hr: Math.floor(Math.random() * (+23 - +21) + +21),
              min: Math.floor(Math.random() * (+59 - +0) + +0)
            },
            shopType: catagories[catagory_iterator]
          });
          shop
            .saveAsync()
            .then(data => {
              console.log("shop", data);
              const id = data._id;
              for (
                let item_iterator = 0;
                item_iterator < item_count;
                item_iterator++
              ) {
                const item_object =
                  catagories[catagory_iterator] !== "Food"
                    ? {
                        _id: new mongoose.Types.ObjectId(),
                        name: faker.commerce.productName(),
                        price: faker.commerce.price(),
                        pic: faker.image.imageUrl(),
                        itemType: "Product",
                        brand:
                          Brands[catagory_iterator][
                            Math.floor(Math.random() * (+5 - +0) + +0)
                          ],
                        shopId: id
                      }
                    : item_iterator <= item_count / 2
                    ? {
                        _id: new mongoose.Types.ObjectId(),
                        name: faker.commerce.productName(),
                        price: faker.commerce.price(),
                        pic: faker.image.imageUrl(),
                        itemType: "Veg",
                        brand: null,
                        shopId: id
                      }
                    : {
                        _id: new mongoose.Types.ObjectId(),
                        name: faker.commerce.productName(),
                        price: faker.commerce.price(),
                        pic: faker.image.imageUrl(),
                        itemType: "NonVeg",
                        brand: null,
                        shopId: id
                      };
                const item = new Item(item_object);
                item
                  .saveAsync()
                  .then(res => {
                    if (
                      shops_iterator === shop_count - 1 &&
                      catagory_iterator === catagories.length - 1 &&
                      item_iterator === item_count - 1
                    ) {
                      callback(null, "Seeding Shops done");
                    }
                  })
                  .catch(err => console.log("error in saving item", err));
              }
            })
            .catch(err => console.log("error in saving shop", err));
        }
      }
    }
  ],
  (err, results) => {
    console.log("\n\n--- Database seed progam completed ---");
    if (err) {
      console.log("Errors = ");
      console.dir(err);
    } else {
      console.log("Results = ");
      console.log(results);
    }
    console.log("\n\n--- Exiting database seed progam ---");
    // Exit the process to get back to terrminal console
    process.exit(0);
  }
);
