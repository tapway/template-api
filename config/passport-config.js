import passportJWT from "passport-jwt";
import cfg from "./env";
import DeliverySchema from "../server/models/deliveryuser";
import EndUserSchema from "../server/models/enduser";

const ExtractJwt = passportJWT.ExtractJwt;
const jwtStrategy = passportJWT.Strategy;

function passportConfiguration(passport) {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
  opts.secretOrKey = cfg.jwtSecret;
  passport.use(
    new jwtStrategy(opts, (jwtPayload, cb) => {
      if (jwtPayload._doc.type == "endUser") {
        EndUserSchema.findOneAsync({ _id: jwtPayload._doc._id })
          .then(user => cb(null, user))
          .error(err => cb(err, false));
      } else {
        DeliverySchema.findOneAsync({ _id: jwtPayload._doc._id })
          .then(user => cb(null, user))
          .error(err => cb(err, false));
      }
    })
  );
}

export default passportConfiguration;
