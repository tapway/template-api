import Joi from "joi";

export default {
  // POST /api/users
  createUser: {
    body: {
      phoneNumber: Joi.string().required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {},
    params: {
      userId: Joi.string()
        .hex()
        .required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      phoneNumber: Joi.string().required()
    }
  },

  // POST /api/auth/login
  admin_login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required()
    }
  },

  // POST /api/auth/otpVeirfy
  otpVerify: {
    body: {
      phoneNumber: Joi.string().required(),
    }
  }
};
