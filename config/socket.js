import jwt from "jsonwebtoken";
import config from "./env";
import sockeHandler from "../server/socketHandler";
import SocketStore from "../server/socketHandler/socketstore";

export default function startSocketServer(server) {
  const io = require("socket.io").listen(server); //eslint-disable-line

  console.log("SocketServer started"); //eslint-disable-line
  io.on("connection", socket => {
    console.log(
      "Client connected to socket",
      socket.id,
      socket.handshake.query.type,
      "@@"
    ); //eslint-disable-line
    const authToken = socket.handshake.query.token.replace("JWT ", ""); // check for authentication of the socket
    const type = socket.handshake.query.type;
    const id = socket.handshake.query.id;
    jwt.verify(authToken, config.jwtSecret, (err, userDtls) => {
      if (err) {
        socket.disconnect();
      } else if (userDtls) {
        socket.userId = userDtls._doc._id; //eslint-disable-line
        if (type == "delivery") {
          SocketStore.addByDeliveryId(id, socket);
        }
        if (type == "user") {
          SocketStore.addByUserId(id, socket);
        }
        sockeHandler(socket, type); // call socketHandler to handle different socket scenario
      }
    });
  });
}
