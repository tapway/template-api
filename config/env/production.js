export default {
  env: "production",
  jwtSecret: "SECRET",
  db: "mongodb://localhost/delivery-production",
  port: 4020,
  radius: 30 / 6378,
  passportOptions: {
    session: false
  },
};

// test
