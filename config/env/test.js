export default {
  env: "test",
  jwtSecret: "SECRET",
  db: "mongodb://localhost/delivery",
  port: 4020,
  radius: 30 / 6378,
  passportOptions: {
    session: false
  },
};
