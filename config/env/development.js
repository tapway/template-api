export default {
    env: "development",
    jwtSecret: "SECRET",
    //db: "mongodb://localhost/delivery",
    db: "mongodb://dbuser:dbuser123@ds259245.mlab.com:59245/delivery",
    port: 4020,
    radius: 30 / 6378,
    passportOptions: {
        session: false
    }
};
